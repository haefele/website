Journée en l'honneur des soixante ans de Thierry Dumont
#######################################################

:date: 2010-11-19 11:44:24
:modified: 2010-11-19 11:44:24
:category: journee
:tags: dumont
:place: Lyon
:start_date: 2010-12-17
:end_date: 2010-12-17
:summary: Le Groupe Calcul organise une journée pour les soixante ans de Thierry Dumont


.. contents::

.. section:: Description
    :class: description

    A l'occasion des soixante ans de Thierry Dumont a été organisée une journée scientifique à Lyon, à l'Institut Camille Jordan, salle Fokko du Cloux, le 17 décembre 2010.

    .. figure:: attachments/spip/Documents/Manifestations/TD60/thierry-dumont.JPG
        :height: 200
        :align: center
        :alt: Photo de Thierry Dumont



.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 17-12-2010

            .. break_event:: Accueil
                :begin: 09:00
                :end: 09:30

            .. event:: Pas de titre
                :speaker: Philippe Depouilly (Institut de Mathématiques de Bordeaux) et Joël Marchand (Observatoire de Paris)
                :begin: 09:30
                :end: 10:00


            .. event:: Thierry, l'homme visco-élasto-plastique
                :speaker: Didier Bresch (LAMA, Université de Savoie)
                :begin: 10:00
                :end: 10:30

                Thierry a apporté sa pierre à l'édifice Mathématique-Numérique lié à des modèles pour fluides complexes. Après avoir rappelé ses diverses contributions, cet exposé aura pour but de présenter quelques résultats nouveaux sur ce sujet.

            .. event:: Pas de titre
                :speaker: Max Duarte (EM2C, Ecole Centrale Paris)
                :begin: 10:30
                :end: 11:00

            .. break_event:: Pause
                :begin: 11:00
                :end: 11:20

            .. event:: Quand Thierry se coupe en se rasant le matin
                :speaker: Magali Ribot (Laboratoire J. Dieudonné, Nice)
                :begin: 11:20
                :end: 11:50

            .. event:: Thierry Dumont convoqué par l'AERES (Agence d'Évaluation des Retraites et de l'Économie Sénile)
                :speaker: Pierre Crepel (Institut Camille Jordan, Lyon)
                :begin: 11:50
                :end: 12:35

            .. break_event:: Buffet
                :begin: 12:35
                :end: 13:45


            .. event:: Tableau noir, tableau blanc
                :speaker: Thierry Dumont (Université Lyon 1)
                :begin: 13:45
                :end: 14:15


            .. event:: Thierry, la tête dans les étoiles
                :speaker: Carole Rosier (Université du Littoral)
                :begin: 14:15
                :end: 14:45

      
            .. event:: Du hasard bénin au hasard sauvage
                :speaker: Raoul Robert (UJF, Grenoble)
                :begin: 14:45
                :end: 15:30


            .. event:: Aldil et logiciels libres
                :speaker: Jean-Charles Delépine (Amiens)
                :begin: 15:30
                :end: 15:50

            
            .. break_event:: Pause
                :begin: 15:50
                :end: 16:10


            .. event:: Pas de titre
                :speaker: Marie Aimée Dronne (Université Lyon 1)
                :begin: 16:10
                :end: 16:40

            .. event:: Thierry, éleveur de zèbres
                :speaker: Stéphane Descombes (Laboratoire J. Dieudonné, Nice)
                :begin: 16:40
                :end: 17:10


.. section:: Organisateurs
    :class: orga

    - Stéphane Descombes
    - Violaine Louvet
    - Magali Ribot.

