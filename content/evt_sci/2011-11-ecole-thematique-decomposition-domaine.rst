Ecole thématique "Méthodes de décomposition de domaine : de la théorie à la pratique"
#####################################################################################

:date: 2014-11-05 15:53:49
:modified: 2014-11-05 15:53:49
:category: formation
:place: Fréjus
:start_date: 2011-11-14
:end_date: 2011-11-18
:summary: Une école thématique du GdR Calcul sur les méthodes de décomposition de domaine.


.. contents::

.. section:: Introduction
    :class: description

    Depuis quelques années, le besoin se fait sentir de réaliser la simulation numérique de problèmes de plus en plus complexes, de taille de plus en plus grande.

    La complexité croissante de ces problèmes nécessite des méthodes adaptées, tant du point de vue numérique, que du point de vue informatique au niveau de leur implémentation. Ces deux aspects sont fortement liés.

    Nous proposons, dans cette école thématique, de faire le point sur les méthodes de décomposition de domaine pour la simulation des équations aux dérivées partielles qu'elles soient de type paraboliques ou hyperboliques.

    L'idée de ces méthodes est de diviser le domaine physique en plusieurs sous-domaines de calcul, de faire la résolution des équations sur chacun des sous-domaines et d'échanger les conditions de transmission entre sous-domaines.

.. section:: Objectifs
    :class: description

    Les objectifs de cette école sont les suivants :

    - faire connaître les avancées récentes du calcul scientifique sur ces méthodes,
    - évoquer les questions liées à leur implémentation (structures des données, algorithmes, ...) en lien avec les architectures de calcul émergentes (et notamment le massivement parallèle),
    - aborder ces deux points sur le plan pratique.

    L'école se déroulera du 14 novembre 2010 au 18 novembre 2010 à Fréjus. Elle sera constituée d'une présentation générale des méthodes en décomposition de domaine donnée par Martin Gander, de 2 cours théoriques de 6h donnés notamment par François-Xavier Roux et Victorita Dolean. Ces cours seront suivis par des TPs sur l'implémentation de ces méthodes donnés par Loïc Gouarin et Laurent Series. Il y aura également des exposés scientifiques donnant des applications concrètes de ces méthodes.


.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 14-11-2011

            .. event:: Domain Decomposition Methods: Schwarz, Schur, Waveform Relaxation and the Parareal Algorithm
                :begin: 14:00
                :end: 17:00
                :speaker: Martin Gander  (Université de Genève)
                :support: attachments/spip/Documents/Ecoles/ET2011DD/MGander.pdf

                Domain decomposition methods have been developed in various contexts, and with very different goals in mind. I will start my presentation with the historical inventions of the Schwarz method, the Schur methods and Waveform Relaxation. I will then show for a simple model problem how all these domain decomposition methods function, give precise results for the model problem, and also explain the most general convergence results available currently for these methods. I will conclude with the parareal algorithm and interesting recent variants.

                Content:

                1. Historical introduction:
            
                    - The invention of Schwarz
                    - Przemieniecki and Substructuring
                    - Picard, Lindeloef and Waveform Relaxation

                2. Schwarz Methods for a model problem:

                    - Alternating and parallel Schwarz methods
                    - Multiplicative and additive Schwarz methods, RAS
                    - Schwarz methods as preconditioners
                    - Optimized Schwarz methods

                3. Schur Complement Methods for a model problem:

                    - Primal Schur methods
                    - Dual Schur methods
                    - FETI and Neumann-Neumann
                    - Dirichlet-Neumann and Neumann-Dirichlet

                4. Coarse Grid Corrections:

                    - Scalability Problems
                    - Construction of a coarse grid correction

                5. Space-Time Parallel Methods:

                    - Schwarz waveform relaxation methods
                    - Optimized Schwarz waveform relaxation methods
                    - Time parallelization: the Parareal Algorithm
                    - A general space-time parallel method

        .. day:: 15-11-2011

            .. event:: Méthodes de Schwarz
                :begin: 09:00
                :end: 12:00
                :speaker: Victorita Dolean (Laboratoire J.A. Dieudonné, Nice)
                :support:
                    [Cours 1](attachments/spip/Documents/Ecoles/ET2011DD/VDolean1.pdf)
                    [Cours 2](attachments/spip/Documents/Ecoles/ET2011DD/VDolean2.pdf)

                - Méthodes de Schwarz: au niveau continu + version discrètes: Block-Jacobi, RAS, AS; cas de deux et plusieurs sous-domaines; analyse de convergence geometrique et Fourier; illustrations en utilisant FreeFEM++.
                - Méthodes de Krylov (en bref) et application à la décomposition de domaine.
                - Grilles grossières: besoin d'une méthode à deux niveaux, quelques éléments théoriques des méthodes de Schwarz à deux niveaux et application à un problème elliptique; illustrations en utilisant FreeFEM++.
                - Méthodes de Schwarz "modifiées": Conditions d'interface optimales et optimisées, principes generaux. Application au Laplacien, Helmholtz et éventuellement Maxwell.


        .. day:: 16-11-2011

            .. event:: Méthodes FETI
                :begin: 14:00
                :end: 17:00
                :speaker: François-Xavier Roux (ONERA, Palaiseau)
                :support:
                    [Cours 1](attachments/spip/Documents/Ecoles/ET2011DD/FXRoux1.pdf)
                    [Cours 2](attachments/spip/Documents/Ecoles/ET2011DD/FXRoux2.pdf)

                - La méthode du complément de Schur. Interprétation comme un préconditionneur local optimal. Propriétés du complément de Schur.
                - Approche algébrique générale pour les méthodes de résolution par sous-domaines sans recouvrement.
                - Méthode FETI. Conditions de raccord redondantes aux coins. Préconditionnement Dirichlet.
                - Problème des singularités locales, projeteur « grille grossière » associé.
                - FETI à deux multiplicateurs de Lagrange. Conditions de raccord optimales.
                - Approche générale pour fabriquer des préconditionneurs « grille grossière » par des méthodes de type déflation.


        .. day:: 17-11-2011

            .. event:: Implémentation : méthodes de Schwarz
                :begin: 09:00
                :end: 17:00
                :speaker: Loïc Gouarin (Laboratoire de mathématique d'Orsay)
                :support:
                    [TP](attachments/spip/Documents/Ecoles/ET2011DD/TP_ETDDM2011.pdf)
                    [Correction](attachments/spip/Documents/Ecoles/ET2011DD/TPs-tar.gz)


        .. day:: 18-11-2011

            .. event:: Implementation Méthodes FETI
                :begin: 09:00
                :end: 12:00
                :speaker: Laurent Series (ECP, Châtenay-Malabry)
                :support:
                    [TP](attachments/spip/Documents/Ecoles/ET2011DD/TP_ETDDM2011.pdf)
                    [Correction](attachments/spip/Documents/Ecoles/ET2011DD/TPs-tar.gz)


.. section:: Comité scientifique
    :class: orga

    - Stéphane Descombes (Laboratoire Dieudonné, Nice)
    - Martin Gander (Département de mathématiques, Genève)
    - Loïc Gouarin (Laboratoire de mathématique, Orsay)
    - Laurence Halpern (Laga, Université Paris 13)

.. section:: Comité d'organisation
    :class: orga

    - Stéphane Descombes (Laboratoire Dieudonné, Nice)
    - Thierry Dumont (Institut Camille Jordan, Lyon)
    - Loïc Gouarin (Laboratoire de mathématique, Orsay)
    - Violaine Louvet (Institut Camille Jordan, Lyon)
