Licences et gestion de projets
##############################

:date: 2004-10-14 08:29:19
:start_date: 2004-10-14
:end_date: 2004-10-14
:category: journee
:tags: licence
:place: Lyon, France


.. contents::

.. section:: Description
    :class: description

    Le Groupe Calcul  et l'Unité de Service CODICIEL s'associent pour proposer une session de formation destinée aux acteurs du calcul scientifique et portant sur un certain nombre d'outils pour le développement des codes de calcul dans le cadre de la Fédération Lyonnaise de Calcul Haute Performance.

    Cette formation s'articule entre des exposés généraux et des travaux pratiques sur des outils spécifiques.

.. section:: Date et Lieu
    :class: description

    Le jeudi 14 octobre 2004.

    Université Lyon 1, Bâtiment Omega,UFR de Mécanique, salle DESS.

.. section:: Programme
    :class: programme

    .. schedule::

       .. day:: 14-10-2004

	  .. break_event:: Accueil
	     :begin: 09:30
	     :end: 9:45

	  .. event:: Présentation de l'UPS CODICIEL et du Groupe Calcul
	     :begin: 9:45
	     :end: 10:00

	  .. event:: Licences et Logiciels libres
	     :begin: 10:00
	     :end: 10:30
	     :speaker: Sébastien BESSON (Président de AnimPI, Association des animateurs en propriété intellectuelle)
	     :support: attachments/spip/Documents/Journees/oct2004/Logiciels_libres.pdf

	  .. break_event:: Pause
	     :begin: 10:30
	     :end: 10:40

	  .. event:: La Licence CeCILL: Ce(A)C(nrs)I(NRIA)L(ogiciel)L(ibre)
	     :begin: 10:40
	     :end: 11:10
	     :speaker: Stéphane Dalmas (Chargé de la valorisation et des relations européennes, INRIA Sophia Antipolis)
	     :support: attachments/spip/Documents/Journees/oct2004/cecill.pdf

	  .. event:: Discussion-Débat : Autour de la problématique des licences
	     :begin: 11:10
	     :end: 11:40

 	  .. event:: GNU autotools pour la publication et le déploiement de logiciels
	     :begin: 11:40
	     :end: 12:40
	     :speaker: François GANNAZ (LMC, IMAG)
	     :support: attachments/spip/Documents/Journees/oct2004/autotools.pdf

	  .. break_event:: Repas
	     :begin: 12:40
	     :end: 14:00

	  .. event:: GNU autotools : mise en pratique
	     :begin: 14:00
	     :end: 15:00
	     :speaker: François GANNAZ (LMC, IMAG)
	     :support:
		[Sujet du TP](attachments/spip/Documents/Journees/oct2004/sujet_autotools.txt)
                [Fichier du TP](attachments/spip/Documents/Journees/oct2004/lsavi-1.0.tar.gz)
                [Solution du TP](attachments/spip/Documents/Journees/oct2004/lsavi-1.0-solution.tar.gz)

	  .. break_event:: Pause
	     :begin: 15:00
	     :end: 15:15

	  .. event:: Subversion
	     :begin: 15:15
	     :end: 17:00
	     :speaker:  Thierry DUMONT, Violaine Louvet (UCBL, MAPLY)
	     :support: attachments/spip/Documents/Journees/oct2004/subversion.pdf

	  .. event:: CVS
	     :begin: 15:15
	     :end: 17:00
	     :speaker: Anne CADIOU (ECL, CODICIEL-LMFA)
	     :support:
		[Support](attachments/spip/Documents/Journees/oct2004/cours-cvs.pdf)
		[Mise en pratique](attachments/spip/Documents/Journees/oct2004/exemple_simple.zip)_

	  .. event:: Discussion-Débat : Autour du développement de logiciels pour le calcul scientifique
	     :begin: 17:00
	     :end: 18:00
