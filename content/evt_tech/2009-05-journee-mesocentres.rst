Journée mésocentres |_| 2009
############################

:date: 2009-05-12 06:35:48
:start_date: 2009-09-24
:end_date: 2009-09-24
:category: journee
:tags: mesocentre
:place: Institut Henri Poincaré, Paris

.. contents::

.. section:: Description
    :class: description

    La `CPU <http://www.cpu.fr/>`__, en collaboration avec GENCI et le Groupe Calcul a organisé une journée de rencontres sur/pour les mésocentres le 24 septembre 2009 à l'`IHP (Institut Henri Poincaré)  <http://www.ihp.fr>`__, amphi Hermite, à Paris.

    **Rappel**

    - `Une première journée a eu lieu en février 2008 <2008-02-journee-mesocentres.html>`__.
    - `Listes des mésocentres <pages/mesocentres_en_france.html>`__.

.. section:: Programme
   :class: programme

      .. schedule::

	 .. day:: 24-09-2009

	    .. break_event:: Accueil
	       :begin: 09:15
               :end: 9:30

            .. event:: Introduction
               :begin: 09:30
               :end: 09:40
               :speaker: Daniel Egret (CPU)

	    .. event:: L'évolution des moyens de calcul nationaux et européens (PRACE) et l'articulation avec les mésocentres
               :begin: 09:40
               :end: 10:00
               :speaker: C. Riviere, S. Requena (GENCI)
	       :support: attachments/spip/Documents/Journees/sept2009/GENCI-sept2009.pdf

	    .. event::  Le CSCI
	       :begin: 10:00
               :end: 10:20
               :speaker: Olivier Pironneau (CSCI)
	       :support: attachments/spip/Documents/Journees/sept2009/csci-sept2009.pdf

	    .. event:: Présentation de quatre mésocentres
               :begin: 10:20
               :end: 12:30
               :speaker:
               :support:
		  [CICT-CALMIP](attachments/spip/Documents/Journees/sept2009/calmip-sept2009.pdf)
                  [Strasbourg](attachments/spip/Documents/Journees/sept2009/strasbourg-sept2009.pdf)
                  [CRIHAN](attachments/spip/Documents/Journees/sept2009/crihan-sept2009.pdf)
                  [PSMN](attachments/spip/Documents/Journees/sept2009/psmn-sept2009.pdf)
                  [Centre Blaise Pascal](attachments/spip/Documents/Journees/sept2009/cbp-sept2009.pdf)

               - CICT-CALMIP , Nicolas Renon.
               - Mutualisation à Strasbourg, Romaric David.
               - CRIHAN "Le Pôle Haut Normand de Modélisation Numérique", Patrick Bousquet-Mélou.
               - PSMN, Hervé Gilquin.
               - Centre Blaise Pascal, Ralf Everaers.

	    .. break_event:: Déjeuner
	       :begin: 12:30
               :end: 13:40

            .. event:: Aspects énergétiques/climatisation des salles de calcul
               :begin: 13:40
               :end: 14:00
               :speaker: Françoise Berthoud (CIMENT, Calcul)
	       :support: attachments/spip/Documents/Journees/sept2009/salles-machines-sept2009.pdf

	    .. event:: Grilles de recherche, grilles de production
               :begin: 14:00
               :end: 14:20
               :speaker: David Margery (INRIA, Grid 5000), Dominique Boutigny (CC-IN2P3)
	       :support:
		  [Grille de recherche](attachments/spip/Documents/Journees/sept2009/grid5000-sept2009.pdf)
		  [Grille de production](attachments/spip/Documents/Journees/sept2009/grilles-prod-sept2009.pdf)

	    .. event:: Label C3I
               :begin: 14:20
               :end: 14:40
               :speaker: Stéphane Cordier (MAPMO, Calcul)
	       :support: attachments/spip/Documents/Journees/sept2009/c3i-sept2009.pdf

	    .. break_event:: Pause
	       :begin: 14:40
               :end: 15:10

            .. event:: Evolution du parc des mésocentres (rapports 2009 en ligne).
               :begin: 15:10
               :end: 15:30
               :speaker: Violaine Louvet (Calcul)
	       :support:
		  [Support](attachments/spip/Documents/Journees/sept2009/mesocentres-sept2009.pdf)
		  [Rapport 2009](attachments/spip/Documents/Mesocentres/Rapports/sept2009/mesocentres-2009.pdf)
		  [Rapport détaillé](attachments/spip/Documents/Mesocentres/Rapports/sept2009/mesocentres-details-2009.pdf)

	    .. event:: Le calcul à l'INSMI (INstitut des Sciences Mathématiques et de leurs Interactions, CNRS)
               :begin: 15:30
               :end: 15:50
               :speaker: Guy Métivier

	    .. event:: Table ronde : "Mésocentres : rôle dans le paysage du HPC et évolutions"
               :begin: 15:50
               :end: 17:00
	       :speaker: Michel Kern (INRIA)

	       `Compte-rendu de la journée, dont la table ronde <attachments/spip/Documents/Journees/sept2009/CR_JourneeMesocentes20090924.pdf>`__
