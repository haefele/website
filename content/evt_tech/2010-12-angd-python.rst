ANGD Python en calcul scientifique
##################################

:date: 2010-11-02 14:30:18
:start_date: 2010-12-06
:end_date: 2010-12-10
:category: formation
:place: Autrans, France


.. contents::

.. section:: Description
    :class: description

    Le langage Python est un langage interprété multi-paradigme utilisable dans de très nombreux contextes grâce à des bibliothèques spécialisées. La communauté Python ne cesse de s'élargir en offrant à ce langage de plus en plus de possibilités. Un ensemble d'outils en calcul et en visualisation scientifique permettent d'utiliser le langage Python comme un langage de haut niveau pour le prototypage et l’environnement des codes de calcul.
    Il permet également le développement d'applications performantes « sur mesure » car il est interfaçable facilement avec des routines écrites en langages de bas niveau (Fortran, C ou C++). Ce langage aide ainsi à réaliser rapidement des codes de recherche efficaces.
    De plus, il facilite l'évolution de ces codes vers de vrais logiciels puisqu'il existe un ensemble de modules permettant de faire rapidement des interfaces graphiques, des applications web, ... Les utilisateurs peuvent ainsi interagir sur leurs simulations numériques et visualiser les résultats obtenus de façon confortable.

    Cette formation avait pour but d’expliquer l’intérêt du langage Python dans le domaine du calcul scientifique, dans tous les aspects du développement, du cycle de vie, et de l’utilisation d’un code de calcul.

    Lors de cette formation, nous avons considèré un modèle mathématique issu d'un problème le plus multidisciplinaire possible. Il a  servit de fil conducteur pour développer un code de calcul le plus abouti  possible, c’est-à-dire avec pré et post-traitement des données et packaging.

    La formation s'est déroulée sous forme de cours théoriques qui ont été mis en pratique lors de TP. Cela a permis de parcourir tous les stades de conception d'un tel logiciel et de voir à chaque étape quels sont les modules Python les plus adaptés.

    Les points abordés lors de cette formation ont été les suivants:

        - Les principaux concepts du langage Python.
        - Les modules Python dédiés au calcul: manipulation de tableaux multidimensionnels (numpy), bibliothèques scientifiques (scipy).
        - Les outils de debogage et de profilage.
        - L'interfaçage Python avec des routines écrites en langages bas niveaux : Fortran (f2py) et C/C++(swig), pour avoir des codes performants et réutiliser les sources existantes.
        - Visualisation des résultats numériques obtenus en temps réel (vtk, matplotlib, VPython et Qwt), ou à l’issu du calcul (interfaçage de Paraview).
        - Le packaging.
        - Les outils de parallélisation.



.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 06-12-2010

                .. event:: Présentation générale de Python
                    :begin: 09:00
                    :end: 10:00
                    :speaker: Loïc Gouarin
                    :support:
                        [Support du cours](attachments/spip/Documents/Ecoles/2010/les_bases.pdf)
                        [Vidéo du cours](attachments/spip/Documents/Ecoles/2010/Videos/Videos_br/Python_Les_bases.mp4)

                    - Bref historique
                    - Que peut-on faire avec Python ?
                    - L'interpréteur de base et IPython
                    - Les types de base
                    - Les modules
                    - Les entrées/sorties
                    - Les classes
                    - ...

                .. event:: Cours Numpy
                    :begin: 10:15
                    :end: 11:15
                    :speaker: Marc Poinot
                    :support:
                        [Support du cours](attachments/spip/Documents/Ecoles/2010/cnrs-python-poinot-numpy.pdf)
                        [Vidéos du cours](attachments/spip/Documents/Ecoles/2010/Videos/Videos_br/Python_Numpy.mp4)

                    - Introduction :

                        - historique
                        - contexte calcul intensif
                        - migration numarray ?

                    - Manipulations de base :

                        - création
                        - indexing/slicing
                        - quelques méthodes

                    - Détails des tableaux :

                        - shape, dtype, order
                        - I/O
                        - print options
                        - masked arrays
                        - fonctions utilisateur "UFuncs"
                        - extension de types

                    - Divers :

                        - quelques packages (random, matrices, strings, ...)
                        - gestion des erreurs
                        - méthodes et fonctions supplémentaires

                .. event:: Cours Scipy
                    :begin: 11:30
                    :end: 12:30
                    :speaker: Sylvain Faure
                    :support:
                        [Support du cours](attachments/spip/Documents/Ecoles/2010/cours_Python_Scipy.pdf)
                        [Vidéos du cours](attachments/spip/Documents/Ecoles/2010/Videos/Videos_br/Python_Scipy.mp4)

                    - Présentation générale du module et de ce qu'il contient
                    - Présentation plus détaillée du contenu de quelques sous-modules :

                        - interpolation
                        - FFT
                        - intégration (résolution ODE)
                        - algèbre linéaire
                        - matrices creuses
                        - optimisation

                .. event:: TP sur les bases de Python, Numpy et scipy
                    :begin: 14:00
                    :end: 17:30
                    :speaker: Sylvain Faure, Loïc Gouarin, Konrad Hinsen, Marc Poinot
                    :support:
                        [TP système solaire](attachments/spip/Documents/Ecoles/2010/tp-numpy-systeme-solaire.tar.gz)
                        [TP équation de poisson](attachments/spip/Documents/Ecoles/2010/TP-numpy.tar)
                        [Énoncé](attachments/spip/Documents/Ecoles/2010/cnrs-python-poinot-numpy-TP.pdf)
                        [TP Scipy](attachments/spip/Documents/Ecoles/2010/DiffusionWithScipy.tgz)
                        [données](attachments/spip/Documents/Ecoles/2010/img.txt)

            .. day:: 07-12-2010

                .. event:: Cours Matplotlib, PyQwt, guiqwt
                    :begin: 08:00
                    :end: 09:30
                    :speaker: Pierre Raybaut
                    :support:
                        [Support du cours](attachments/spip/Documents/Ecoles/2010/cours_visu2d_python-v2.pdf)
                        [Vidéos du cours](attachments/spip/Documents/Ecoles/2010/Videos/Videos_br/Python_Matplotlib_PyQwt_guiqwt.mp4)

                    - Introduction: tour d'horizon des modules Python de visualisation (2D et 3D)
                    - Matplotlib: visualisation 2D (et 3D) interactive
                    - PyQwt: visualisation performante bas niveau
                    - guiqwt (avec guidata) :

                        - visualisation performante haut niveau
                        - aide à la conception de logiciels de traitement du signal et de l'image

                .. event:: Cours VTK, paraview
                    :begin: 09:45
                    :end: 10:45
                    :speaker: Sylvain Faure
                    :support:
                        [Support du cours](attachments/spip/Documents/Ecoles/2010/cours_python_vtk.tar.gz)
                        [Vidéos du cours](attachments/spip/Documents/Ecoles/2010/Videos/Videos_br/Python_VTK.mp4)

                    - Présentation de la librairie graphique VTK
                    - Lecture des données et formats de fichiers de données
                    - Pipeline graphique et filtres
                    - Exemples d'utilisation de quelques filtres :

                        - champ de vecteurs
                        - plans de coupe
                        - isosurfaces
                        - rendu volumique
                        - ...

                .. event:: TP Matplotlib, PyQwt, guiqwt
                    :begin: 11:00
                    :end: 12:30
                    :speaker: Romaric David, Pierre Raybaut
                    :support:
                        [Support du cours](attachments/spip/Documents/Ecoles/2010/matplotlibwidget.py)
                        [Données](attachments/spip/Documents/Ecoles/2010/img.txt)

                .. event:: TP VTK, paraview
                    :begin: 11:00
                    :end: 12:30
                    :speaker: Sylvain Faure, Loïc Gouarin
                    :support:
                        [TP système solaire](attachments/spip/Documents/Ecoles/2010/avec_numpy.py)
                        [Autre source](attachments/spip/Documents/Ecoles/2010/support.py)
                        [Corrigé du TP vtk](attachments/spip/Documents/Ecoles/2010/tp_vtk.tar.gz)

                .. event:: Spyder et Python(x,y)
                    :begin: 14:00
                    :end: 14:45
                    :speaker: Pierre Raybaut

                .. event:: Cours débogage, profilage
                    :begin: 15:00
                    :end: 15:45
                    :speaker: Konrad Hinsen
                    :support:
                        [Support du cours](attachments/spip/Documents/Ecoles/2010/debogage.pdf)
                        [Vidéo du cours](attachments/spip/Documents/Ecoles/2010/Videos/Videos_br/Python_Debogage_Profilage.mp4)

                    - Techniques de débogage
                    - Présentation du débogeur PDB
                    - Analyse post-mortem
                    - Point d'arrêt
                    - Observation de variables
                    - Stratégies de débogage

                .. event:: TP débogage, profilage
                    :begin: 16:00
                    :end: 18:00
                    :speaker: Romaric David, Konrad Hinsen, Marc Poinot
                    :support:
                        [Support du TP](attachments/spip/Documents/Ecoles/2010/TP_debogage_python.tgz)


            .. day:: 08-12-2010

                .. event:: Cours API C, Swig
                    :begin: 09:00
                    :end: 10:00
                    :speaker: Xavier Juvigny
                    :support:
                        [Support du cours](attachments/spip/Documents/Ecoles/2010/InterfacagePython.pdf)
                        [Vidéo du cours](attachments/spip/Documents/Ecoles/2010/Videos/Videos_br/Python_APIC_Swig.mp4)

                    - Présentation d'un problème type : calcul des temps de vol des suites de Syracuse
                    - Lenteur du langage Python : motivation pour interfacer avec un autre langage
                    - Présentation du module ctypes et sa performance
                    - Bref rappel sur f2py et comparaison des temps avec les approches précédentes
                    - Présentation et optimisation du problème avec Swig
                    - Inconvénient de Swig (contrôle de fin de la durée de vie ou du comportement des objets impossible, etc)
                    - Présentation API C via le problème modèle et performance
                    - Utilisation de l'API Numpy avec l'API C et les problèmes de cycle de vie des tableaux
                    - Création de nouvelles classes via l'API C et définition des opérateurs
                    - Gestion de l'héritage
                    - Conclusions

                .. event:: Cours f2py
                    :begin: 10:15
                    :end: 11:00
                    :speaker: Pierre Navaro
                    :support:
                        [Support du cours](attachments/spip/Documents/Ecoles/2010/f2py-cours.pdf)
                        [Vidéo du cours](attachments/spip/Documents/Ecoles/2010/Videos/Videos_br/Python_f2py.mp4)

                    - Pourquoi interfacer Python avec Fortran ?
                    - Quels outils pour interfaçage Python-Fortran ?
                    - Prérequis pour l'utilisation de f2py.
                    - Première interface.
                    - Appel des fonctions f2py dans Python.
                    - Ajouter une variable de sortie calculée par une subroutine fortran.
                    - Documentation des fonctions interfacées.
                    - Les directives pour améliorer l'interface.
                    - La compilation d'une application mixte Python/Fortran.
                    - Créer et éditer les fichiers signature.
                    - Interfaçage avec le langage C.
                    - Comment gérer un module Fortran 90 ?
                    - Encapsulation de données et subroutines Fortran dans un objet Python.
                    - Bonus: Implémenter des subroutines Fortran dans un notebook SAGEMATH.

                .. event:: Cours Cython
                    :begin: 11:15
                    :end: 12:00
                    :speaker: Konrad Hinsen
                    :support:
                        [Support du cours](attachments/spip/Documents/Ecoles/2010/cours_cython.pdf)
                        [Exemples](attachments/spip/Documents/Ecoles/2010/cython_examples.tar.gz)
                        [Vidéo du cours](attachments/spip/Documents/Ecoles/2010/Videos/Videos_br/Python_Cython.mp4)

                    - Le rôle des modules d'extension en Python: optimisation et interfaçage.
                    - Présentation du langage Cython.
                    - L'optimisation ligne par ligne.
                    - L'interfaçage avec du code en C.
                    - Travailler avec des tableaux NumPy.
                    - Classes et types d'extension: les avantages du C++ sans ses désagréments.
                    - Contourner le GIL pour profiter des processeurs multicoeurs.

                .. event:: TP API C, Swig
                    :begin: 14:00
                    :end: 17:00
                    :speaker: Xavier Juvigny, Marc Poinot

                .. event:: TP f2py
                    :begin: 14:00
                    :end: 17:00
                    :speaker: Romaric David, Pierre Navaro
                    :support:
                        [Support du TP](attachments/spip/Documents/Ecoles/2010/tp_f2py.tgz)

                .. event:: TP Cython
                    :begin: 14:00
                    :end: 17:00
                    :speaker: Konrad Hinsen
                    :support:
                        [Support du TP](attachments/spip/Documents/Ecoles/2010/TP_cython.tar.gz)

                .. event:: Cours Sphinx
                    :begin: 17:15
                    :end: 18:00
                    :speaker: Marc Poinot
                    :support:
                        [Support du cours](attachments/spip/Documents/Ecoles/2010/cnrs-python-poinot-sphinx.pdf)
                        [Vidéo du cours](attachments/spip/Documents/Ecoles/2010/Videos/Videos_br/Python_Sphinx.mp4)

                    - Introduction :

                        - Principe
                        - ReST

                    - Fonctions :

                        - Architecture de la documentation
                        - Directives
                        - Production HTML et Latex

                    - Mise en oeuvre :

                        - Package Python
                        - C / C++ / Fortran
                        - Exemples

            .. day:: 09-12-2010

                .. event:: Cours multi-processing
                    :begin: 09:00
                    :end: 10:00
                    :speaker: Konrad Hinsen
                    :support:
                        [Support du cours](attachments/spip/Documents/Ecoles/2010/cours_multiprocessing.pdf)
                        [Exemples](attachments/spip/Documents/Ecoles/2010/multiprocessing_examples.tar.gz)
                        [Vidéo du cours](attachments/spip/Documents/Ecoles/2010/Videos/Videos_br/Pyhon_Multiprocessing.mp4)

                    - Le parallélisme en Python
                    - Le problème du GIL
                    - Présentation du module multiprocessing
                    - Stratégies de parallélisation
                    - L'avenir : futures

                .. event:: Cours MPI
                    :begin: 10:30
                    :end: 11:30
                    :speaker: Loïc Gouarin
                    :support:
                        [Support du cours](attachments/spip/Documents/Ecoles/2010/coursMPI.pdf)
                        [Vidéo du cours](attachments/spip/Documents/Ecoles/2010/Videos/Videos_br/Python_MPI.mp4)

                    - Présentation des différents modules permettant d'utiliser MPI
                    - Présentation détaillée de mpi4py :

                        - nombre de processus, rang d'un processus
                        - communications point à point
                        - communications collectives

                    - Exemple détaillé

                .. event:: TP multi-processing
                    :begin: 14:00
                    :end: 17:00
                    :speaker: Sylvain Faure, Konrad Hinsen
                    :support:
                        [Support du TP](attachments/spip/Documents/Ecoles/2010/TP_multiprocessing.tar.gz)
                        [Numpy shared memory](attachments/spip/Documents/Ecoles/2010/numpy-sharedmem-2009-03-15.tar.gz)


                .. event:: TP MPI
                    :begin: 14:00
                    :end: 17:00
                    :speaker: Romaric David, Loïc Gouarin
                    :support:
                        [Support du TP](attachments/spip/Documents/Ecoles/2010/TP_mpi4py.pdf)
                        [Corrigé du TP](attachments/spip/Documents/Ecoles/2010/Diffusion_MPI.tgz)

                .. event:: Sage
                    :begin: 17:00
                    :end: 18:00
                    :speaker: Thierry Dumont
                    :support:
                        [Support du cours](attachments/spip/Documents/Ecoles/2010/sage.pdf)
                        [Vidéo de la présentation](attachments/spip/Documents/Ecoles/2010/Videos/Videos_br/Python_Sage.mp4)


            .. day:: 10-12-2010

                .. event:: Cours packaging et tests unitaires
                    :begin: 09:00
                    :end: 10:30
                    :speaker: Loïc Gouarin, Xavier Juvigny
                    :support:
                        [Support du cours packaging](attachments/spip/Documents/Ecoles/2010/DistUtilsScons.pdf)
                        [Vidéo du cours](attachments/spip/Documents/Ecoles/2010/Videos/Videos_br/Python_DistUtilsScons.mp4)
                        [Support du cours test unitaires](attachments/spip/Documents/Ecoles/2010/pythonTesting.pdf)
                        [Vidéo du cours](attachments/spip/Documents/Ecoles/2010/Videos/Videos_br/Python_Testing.mp4)

                    - distutils : Un utilitaire python pour produire et distribuer despackages python

                        - Qu'est-ce qu'un package ?
                        - Contrôle de la compilation des modules C/Fortran
                        - Gestion de Numpy

                    - Utilitaire Scons : basé sur le langage python

                        - Scons, un langage descriptif
                        - Exemple de compilation d'un programme Fortran, C
                        - Utilisation des distutils au sein de Scons

                    - Tests unitaires

                        - Présentation du module unittest
                        - Présentation du module nose

                .. event:: TP packaging et tests unitaires
                    :begin: 11:00
                    :end: 12:30
                    :speaker: Loïc Gouarin, Xavier Juvigny
                    :support:
                        [Support du TP packaging](attachments/spip/Documents/Ecoles/2010/DistUtilsTP.tgz)
                        [Support du TP SCons](attachments/spip/Documents/Ecoles/2010/SConsTP.tgz)



.. section:: Organisation
    :class: orga

    - Romaric David (Direction Informatique, Strasbourg)
    - Thierry Dumont (Institut Camille Jordan, Lyon)
    - Sylvain Faure (Maths, Orsay)
    - Konrad Hinsen (Centre de Biophysique Moléculaire, Orléans)
    - Loïc Gouarin (Maths, Orsay)
    - Xavier Juvigny (ONERA, Châtillon)
    - Pierre Navaro (IRMA, Strasbourg)
    - Violaine Louvet (Institut Camille Jordan, Lyon)
    - Marc Poinot (ONERA, Châtillon)
    - Pierre Raybaut (CEA, Paris)
    - Eric Sonnendrucker (IRMA, Strasbourg)
