École Méthodologie et |_| outils d’optimisation en |_| développement logiciel
#############################################################################
:date: 2011-06-13 09:43:25
:category: formation
:start_date: 2012-02-06
:end_date: 2012-02-10
:place: Fréjus

.. contents::

.. section:: Description
  :class: description

  **Les enjeux**
  
  Face au besoin croissant de puissance de calcul et à des applications numériques toujours plus complexes, que cette puissance de calcul soit fournie par des ordinateurs séquentiels ou parallèles, il est nécessaire de pouvoir mieux contrôler l'efficacité et la performance de nos applications.
  L’enjeu principal de cette école est d’être capable d’identifier les points critiques d'une application et de son environnement d'exécution pour, à l'aide d'outils adaptés, améliorer son fonctionnement et son utilisation.
  
   
  **Objectifs pédagogiques de l'école**
  
  Organisée en collaboration avec le Groupe Calcul, elle doit permettre d'acquérir une vision claire de la notion de performance d'un code logiciel, en terme d'environnement, d'architecture et d'application. L'objectif est d'apprendre à mesurer cette performance, d'identifier les bonnes pratiques d'optimisation et de se familiariser avec les principaux outils disponibles dans nos environnements. D'une durée de 4 jours, elle s'adresse principalement aux développeurs, mais intéressera également les ASR pour les aspects environnementaux et architecturaux.
  
   
  **Thèmes abordés**
  
  - Enjeux et défis
  - Catégorisation des problèmes concernés
  - Optimisation des environnements
  - Optimisation des applications
  - Familiarisation avec les principaux outils de profilage
  - Optimisation en environnement multiprocesseurs et/ou multicoeurs
  - Optimisation des I/O et des accès réseaux
  
   
  **Public**
  
  Le public ciblé en premier lieu est celui des ingénieurs ou experts en développement et déploiement d'applications (impliqués dans le support aux expériences en Physique des Hautes Énergies).
  Cette école s'adresse également aux ASRs intéressés par la problématique et confrontés aux interrogations des développeurs.
  
  **Pré requis**
  
  - Les participants devront savoir programmer en C/C++ pour profiter des TDs.
  - Il serait préférable qu'ils aient aussi des rudiments en matière d'architecture système.
  
   
  **Conséquences attendues**
  
  - Transfert de méthodes et techniques
  - Rencontres et échanges entre l'IN2P3 et le groupe Calcul et au sein des groupes eux-mêmes
  - Identification de besoins en matière de formations complémentaires
  
   
.. section:: Programme
  :class: programme

  .. schedule::

    .. day:: 06-02-2012

      .. event:: INTRODUCTION TO PERFORMANCE ANALYSIS
        :begin: 14:30
        :end: 15:30
        :speaker: W. Jalby
        :support: attachments/spip/IMG/pdf/Frejus_part1_V2.pdf

      .. event:: OS IMPACT ON PERFORMANCE
        :begin: 15:30
        :end: 16:30
        :speaker: S. Valat
        :support: attachments/spip/IMG/pdf/frejus-2012-valat.pdf

      .. event:: A QUANTITATIVE APPROACH TO PERFORMANCE OPTIMIZATION
        :begin: 17:00
        :end: 18:00
        :speaker: W. Jalby
        :support: attachments/spip/IMG/pdf/DECAN.pdf

    .. day:: 07-02-2012

      .. event:: Benchmarking: first step toward code optimization
        :begin: 08:45
        :end: 12:15
        :speaker: L. Saugé
        :support: attachments/spip/IMG/zip/IN2P3_Frejus2012_benchmarking.zip

          Evaluate the peak performance of a system and the knowledge of the limitations/constraints of a particular architecture is the first step for the performance analysis of a code and therefore, toward code optimization.
          In this presentation, we study how to evaluate (or how to "benchmark") a high performance computing systems. We will review which hardware (and also software) elements composed such type of systems and for ea ch of them, how it can impact the performances of the code. And from these analyses  we will find best practices the programmer concerned by the performance of his code.
        
      .. event:: Optimization in C & C++
        :begin: 14:00
        :end: 17:30
        :speaker: S. Binet
        :support: 
          [support du cours](attachments/spip/IMG/pdf/20120207-binet-opt-cxx.pdf)
          [support du TP](attachments/spip/IMG/pdf/20120207-binet-tools.pdf)

          Understanding what are the costs of various C/C++ code constructs is
          paramount in eventually providing an application asymptotically reaching
          its full performance potential.
          This lecture will first briefly introduce performance concepts in the
          multicore/manycore landscape and how to detect and assess performance
          issues.
          A non-exhaustive list of various source code optimization techniques for
          C and C++ will follow, associated with their cost.

    .. day:: 08-02-2012

      .. event:: Analysis and Optimization of the Memory Access Behavior of Applications
        :begin: 08:45
        :end: 12:15
        :speaker: J. Weidendorfer
        :support: 
          [support du cours](attachments/spip/IMG/pdf/Valgrind_weidendorfer.pdf)
          [support du TP](attachments/spip/IMG/gz/valgrind_examples-tar.gz)

          The goal of this session is to understand how accessing memory on
          modern CPUs in the wrong way can slow down an application, and
          what can be done to make it faster. This will include a short
          introduction on how caches work in modern CPUs. Standard cache
          optimization strategies such as blocking and padding are
          described, using small example codes such as matrix multiplication
          and stencil codes.
          In the second part, tools are presented which are able to detect
          bad memory access behavior, using hardware performance counters
          as well as cache simulation for detailed analysis. The session
          ends with a hands-on part, demonstrating the use of the tools.

      .. event:: Description of analysis tools
        :begin: 14:00
        :end: 17:30
        :speaker: A. Charif Rubial
        :support: attachments/spip/IMG/pptx/Performance-A_O.pptx

          Static Performance Model :code quality and vectorization opportunities
          Application profiling from corse to fine grain : MIL
          Memory behavior analysis of applications
          Using hardware performance counters

    .. day:: 09-02-2012

      .. event:: Studying the behavior of parallel applications and identifying bottlenecks by using performance analysis tools
        :begin: 08:45
        :end: 15:30
        :speaker: G. Markomanolis
        :support: 
          [support du cours](attachments/spip/IMG/pdf/presentationTAU.pdf)
          [support du TP](attachments/spip/IMG/gz/tutorialTAU-tar.gz)

          Concerning the increase of the available number of the processors and the architecture complexity it becomes
          more difficult to understand how a parallel application is behaving. Many times a parallel application does not
          provide the expected speedup during the execution on many processors. TAU and Scalasca are two performance
          analysis tools that can help the user to identify performance bottlenecks. Each one provides some different tools in
          order to observe a problem that it can be caused by many different factors. Various aspects are covered, instrumenting,
          measurement either profiling or tracing with PAPI hardware counters, analysis and visualization. Moreover an introduction
          into two another tools will be given, the first one is the Score-P which is the next generation of the performance
          measurement infrastructure with the collaboration of the Scalasca, VampirTrace, TAU and some other teams. Finally,
          PerfExpert is a performance diagnosis tool which can provide detailed information about the causes of the bottleneck and
          in some cases to propose optimizations to alleviate the identified bottlenecks.

      .. event:: Optimisation des entrées/sorties pour les systèmes POSIX
        :begin: 16:00
        :end: 17:30
        :speaker: L. Tortay et E. Legay
        :support: 
          [support du cours](attachments/spip/IMG/pdf/ecole-optimisation-in2p3--entrees-sorties--loic-tortay--09-02-2012.pdf)
          [support du TP](attachments/spip/IMG/gz/TP_IO-tar.gz)

          Présentation/rappels de fondamentaux sur les moyens actuels de stockage persistent.  Fonctions des systèmes POSIX (et Linux) pour les entrées/sorties disque & réseau.
          
          Bonnes pratiques pour une utilisation efficace de ces fonctions (« quand, comment, pourquoi les utiliser »).
          
          Présentation d'outils utiles pour déterminer le type d'entrée/sorties effectives d'un programme, en particulier SystemTap (pour Linux).
        
    .. day:: 10-02-2012

      .. event:: Optimisation des entrées/sorties pour les systèmes POSIX
        :begin: 08:00
        :end: 11:30
        :speaker: L. Tortay et E. Legay
        :support: 
          [support du cours](attachments/spip/IMG/pdf/ecole-optimisation-in2p3--entrees-sorties--loic-tortay--09-02-2012.pdf)
          [support du TP](attachments/spip/IMG/gz/TP_IO-tar.gz)

          Présentation/rappels de fondamentaux sur les moyens actuels de stockage persistent.  Fonctions des systèmes POSIX (et Linux) pour les entrées/sorties disque & réseau.
          
          Bonnes pratiques pour une utilisation efficace de ces fonctions (« quand, comment, pourquoi les utiliser »).
          
          Présentation d'outils utiles pour déterminer le type d'entrée/sorties effectives d'un programme, en particulier SystemTap (pour Linux).

.. section:: Comité d'organisation
  :class: orga

  * Romaric David (Unistra)
  * Loïc Gouarin (Lab. Maths U. Paris-Sud)
  * \D. Chamont (LLR)
  * \E. Legay (CSNSM)
  * \P. Micout (IRFU)
  * \A. Pérus (LAL)

