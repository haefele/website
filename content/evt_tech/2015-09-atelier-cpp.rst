Atelier |_| C++ |_| : les |_| bases du |_| 11 |_| et |_| du |_| 14
##################################################################

:date: 2015-06-02 15:23:39
:category: formation
:start_date: 2015-09-08
:end_date: 2015-09-10
:place: Besançon, France
:tags: c++
:summary: Atelier sur les bases de la programmation C++11 et 14 à Bensançon par Joel Falcou du 08/09/2015 au 10/09/2015.


.. contents::

.. section:: Description
    :class: description

    Fort du succès de l'atelier C++ d'avril à `Paris <2015-04-atelier-cpp.html>`_, le groupe Calcul (en partenariat avec le Laboratoire de Mathématiques de Besançon) propose une formation pour débutant en programmation C++11 et 14 les **8, 9 et 10 septembre 2015** à Besançon.

    L'intervenant principal est `Joël Falcou <https://www.lri.fr/~falcou/>`__ secondé (pour les TP) par `Florent Langrognet <https://lmb.univ-fcomte.fr/Florent-Langrognet>`__.

    L'unique prérequis pour ce cours est d'avoir des connaissances en programmation.

    La formation aura lieu dans la Salle A du `Centre Diocésain, 20 rue Mégevand, Besançon <https://www.espacegrammont.fr/>`__

    Les participants devront se munir d'un ordinateur portable.

    - Si l'ordinateur est sous linux : l'installation de g++ 4.8+ ou clang 3.4+ est nécessaire (et suffisante)
    - Sinon, les participants travailleront sur une machine virtuelle qu'il convient d'installer avant. 


    Les supports de cours et de TP peuvent être téléchargés `ici <attachments/spip/IMG/zip/formation_base_cpp.zip>`__

    Des tutoriaux C++ sont aussi disponibles `ici <http://github.com/serge-sans-paille/land_of_cxx>`__

.. section:: Références
    :class: description

    - Sur le web :

        - http://fr.cppreference.com/w/ et http://en.cppreference.com/w/
        - http://herbsutter.com/

    - Livres :

        - "Programming: Principles and Practice Using C++" - Bjarne Stroustrup (2014)
        - "Effective C++", Scott Meyers (2014)
        - "C++ Concurrency", Anthony Williams (2012)
        - "Accelerated C++", Andrew Koenig (2000)

.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 08-09-2015

                .. event:: Introduction
                    :begin: 10:00
                    :end: 12:00
                    :speaker: Joël Falcou (LRI), Florent Langrognet (LMB)


                    - Historique du langage
                    - Pourquoi C++ ?
                    - L'héritage du C
                    - Normes et évolutions

                .. event:: Structures de base d'un programme C++
                    :begin: 14:00
                    :end: 15:30
                    :speaker: Joël Falcou (LRI), Florent Langrognet (LMB)

                    - Types et variables
                    - Structures de contrôle
                    - Fonctions et procédures
                    - Processus de compilation
                    - Mise en pratique

                .. event:: Aspects impératif
                    :begin: 16:00
                    :end: 17:30
                    :speaker: Joël Falcou (LRI), Florent Langrognet (LMB)

                    - Définir une fonction
                    - Paramètres, arguments et valeur de retour
                    - Inférence de type
                    - Gestion des erreurs
                    - Mise en pratique

            .. day:: 09-09-2015

                .. event:: Entrées/Sorties
                    :begin: 09:00
                    :end: 12:00
                    :speaker: Joël Falcou (LRI), Florent Langrognet (LMB)

                    - Notions de flux
                    - Entrées et sorties standards
                    - Mise en pratique

                .. event:: La bibliothèque standard
                    :begin: 14:00
                    :end: 17:30
                    :speaker: Joël Falcou (LRI), Florent Langrognet (LMB)

                    - Conteneurs
                    - Algorithme
                    - Fonctions mathématiques
                    - Dates et heures
                    - Expressions régulières
                    - Mise en pratique

            .. day:: 10-09-2015

                .. event:: Programmation orientée objets
                    :begin: 09:00
                    :end: 12:00
                    :speaker: Joël Falcou (LRI), Florent Langrognet (LMB)

                    - Principes généraux
                    - Notion d'interface
                    - Héritage
                    - Principes de substitution de Liskov
                    - Mise en pratique

                .. event:: Gestion des ressources systèmes
                    :begin: 14:00
                    :end: 15:30
                    :speaker: Joël Falcou (LRI), Florent Langrognet (LMB)

                    - Principe de la RAII
                    - Sémantique de valeur, sémantique d'entité
                    - Pointeurs à sémantique riche
                    - Mise en pratique

                .. event:: Programmation générique
                    :begin: 16:00
                    :end: 17:00
                    :speaker: Joël Falcou (LRI), Florent Langrognet (LMB)

                    - Fonctions génériques
                    - Structures génériques
                    - Mise en pratique

