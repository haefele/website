ANF Visualisation et Données
############################

:date: 2016-09-13 11:48:23
:category: formation
:authors: Sylvain Faure
:start_date: 2016-11-28
:end_date: 2016-12-02
:place: Orsay et Saclay
:summary: Bonnes pratiques pour la production de données et la visualisation


.. contents::

.. section:: Description
    :class: description

    Cette semaine de formation aura pour principaux objectifs :
    - D'initier aux bonnes pratiques concernant la production de données : formats d'archivage, technique d'analyse, cycle de vie...
    - De permettre de maîtriser des outils de visualisation avancés (visualisation in-situ, temps réel, web).

    **Dates et lieu**

    Du lundi 28 novembre (13h45) au vendredi 2 décembre (12h30), Université Paris-Sud et `Maison de la Simulation <http://www.maisondelasimulation.fr/venir-chez-nous>`_

    Université Paris-Sud, Laboratoire de Mathématiques d'Orsay, bâtiment 425, Petit Amphi.
    https://www.math.u-psud.fr/Acces-130

    **Frais d'inscription**

    - Aucun frais d'inscription (sauf pour les entreprises via CNRS Formation Entreprises, nous contacter).
    - Les déjeuners de mardi, mercredi et jeudi seront offerts ainsi que les pauses cafés.
    - Pour les agents CNRS les frais de mission seront intégralement pris en charge.
    - Pour les agents non CNRS ces frais doivent être pris en charge par la tutelle ou le laboratoire de l’agent.

    **Prérequis**

    - Connaissance d’un ou plusieurs langages de programmation (Python; C/C++ ou Fortran).
    - Pratique quotidienne de la programmation.

.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 28-11-2016

            .. event:: Accueil
                :begin: 13:30
                :end: 14:00

            .. event:: Exposé des problématiques des participants
                :begin: 14:00
                :end: 14:30

            .. event:: Mise en œuvre de bonnes pratiques pour la production et l'analyse des données dans son laboratoire
                :begin: 14:30
                :end: 15:45
                :speaker: Cédric Gendre (INRA)
                :support: attachments/spip/IMG/pdf/anf_cedric_gendre.pdf

            .. event:: Cycle de vie des données : exemple et application
                :begin: 16:15
                :end: 17:30
                :speaker: Gabriel Moreau (CNRS & LEGI)
                :support: attachments/spip/Documents/Ecoles/2016/visu/anf_gabriel_moreau.pdf

        .. day:: 29-11-2016

            .. event:: Cours/TP Formats HDF5
                :begin: 09:30
                :end: 10:45
                :speaker: Matthieu Haefele (CNRS & Maison de la Simulation)
                :support: attachments/spip/IMG/zip/hdf5_hands-on-master.zip

            .. event:: Cours/TP Formats XDMF
                :begin: 11:00
                :end: 12:30
                :speaker: Matthieu Haefele (CNRS & Maison de la Simulation)
                :support: attachments/spip/IMG/zip/xdmf_hands-on-master.zip

            .. event:: Application de visualisation in-situ à l'IDRIS avec Paraview/Catalyst sur la plateforme de visualisation Mandelbrot à la Maison de la Simulation
                :begin: 14:00
                :end: 15:30
                :speaker: Martial Mancip (CNRS & Maison de la Simulation)
                :support: attachments/spip/IMG/gz/paraview-insitu-poincare.tar.gz

            .. event:: pyCGNS et CGNS/HDF5, spécialisation de HDF5 pour les données en CFD
                :begin: 16:00
                :end: 17:30
                :speaker: Marc Poinot (SAFRAN)
                :support: attachments/spip/IMG/pdf/pycgns_marc_poinot.pdf

        .. day:: 30-11-2016

            .. event:: Cours/TP Visualisation scientifique (principes et outils)
                :begin: 09:30
                :end: 10:45
                :speaker: Jean Favre (CSCS)
                :support: attachments/spip/Documents/Ecoles/2016/visu/visit.tar.gz

            .. event:: Cours/TP Visualisation scientifique (méthodes et mise en oeuvre)
                :begin: 11:00
                :end: 12:30
                :speaker: Jean Favre (CSCS)

            .. event:: Cours/TP Visualisation scientifique (couplage aux calculs scientifques)
                :begin: 14:00
                :end: 15:30
                :speaker: Jean Favre (CSCS)

            .. event:: Exemple de couplage code/visualisation
                :begin: 16:00
                :end: 17:30
                :speaker: Anne Cadiou (CNRS & LMFA)
                :support: attachments/spip/Documents/Ecoles/2016/visu/pres_ANF_Visu_cadiou.zip

        .. day:: 01-12-2016

            .. event:: Journée consacrée à la réalisation d'un démonstrateur de visualisation in-situ
                :begin: 09:30
                :end: 17:30
                :speaker: Anne Cadiou (CNRS & LMFA) , Hervé Neau (CNRS & IMFT), Annaïg Pedrono (INP Toulouse & IMFT)
                :support: attachments/spip/IMG/gz/tp.tar.gz

        .. day:: 02-12-2016

            .. event:: Code écrit en python avec une visualisation web (d3.js, three.js)
                :begin: 09:30
                :end: 10:45
                :speaker: Caroline Ramond (Instantané & LMO)

            .. event:: Exemple d'application : acquisition de données capteurs, calcul et visualisation sur une page web en temps réel
                :begin: 11:00
                :end: 12:30
                :speaker: Caroline Ramond (Instantané & LMO)
                :support: attachments/spip/IMG/gz/node.tar.gz

.. section:: Organisation
    :class: orga

    - Anne Cadiou (CNRS & Laboratoire de Mécanique des Fluides et d'Acoustique)
    - Sylvain Faure (CNRS & Laboratoire de Mathématiques d'Orsay)

