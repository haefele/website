Atelier intégration continue
############################

:date: 2017-04-05 21:56:37
:category: formation
:start_date: 2017-05-29
:end_date: 2017-05-31
:place: Paris

.. contents::

.. section:: Description
    :class: description

    Le groupe Calcul propose un atelier de trois jours pour vous sensibiliser aux outils utilisés pour faire de l'intégration continue. Cet atelier se tiendra en salle 314 à l'`IHP <http://www.ihp.fr>`__ à Paris les 29, 30 et 31 mai 2017.

    Lorsqu'on développe un code de calcul, seul ou à plusieurs, il est primordial de vérifier que chaque modification ne produit pas de régression dans l'ensemble de l'application. Il est donc nécessaire d'employer des tests unitaires, des tests d'intégration ou des tests du système complet. Ces tests s'intègrent dans un système de gestion de versions pour sauvegarder les modifications.

    L’intégration continue fournit des outils permettant de valider l’intégrité du code à chaque soumission de modifications via github, gitlab, etc. Si, auparavant, il était assez fastidieux de mettre en oeuvre et d'administrer une chaîne d'intégration continue, les outils actuels sont très faciles à déployer. Ils offrent de plus des fonctionnalités qui permettent d’aller bien plus loin que la simple exécution de tests : la couverture du code, la validation syntaxique, la construction d’images de conteneurs et leur déploiement sur un dépôt, etc.

    Durant cet atelier de trois jours, nous vous familiariserons à trois outils d’intégration continue : Jenkins, Travis CI et Gitlab CI, en commençant par une introduction à deux outils couramment utilisés dans une chaîne d'intégration continue : Git pour gérer les versions et les publier vers un dépôt distant, puis Docker pour exécuter les tâches de compilation et de tests.


.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 29-05-2017

            .. break_event:: Accueil
                :begin: 09:30
                :end: 10:00

            .. event:: Introduction à Git
                :begin: 10:00
                :end: 11:30
                :speaker: Alexandre Ancel (IHU Strasbourg)
                :support: attachments/spip/IMG/pdf/git.pdf

            .. event:: Docker [1/3]
                :begin: 11:45
                :end: 12:30
                :speaker: Johan Moreau (IRCAD) et Alexandre Ancel (IHU Strasbourg)
                :support: attachments/spip/IMG/pdf/170518_rd_ci.pdf

                Depuis quelques années, les méthodes agiles ont fait évoluer la manière de réaliser des logiciels. Entre les valeurs (communication, simplicité, …) et les pratiques (petites livraisons, tests, …) de ces méthodes, l’intégration continue joue un rôle fondamental dans ces processus. Se basant sur la possibilité d’accéder au code à tout moment et la possibilité de construire l’application de manière automatique, cet outil devient le chef d’orchestre de la cadence de développement. De nombreux outils existent depuis quelques années, mais un changement récent dans les infrastructures informatiques redistribuent profondément les cartes. En effet l’arrivée des conteneurs logiciels modifient la manière de créer des applications, avec la notion d’architecture micro-services, et donc par effet de bord, la manière donc l’intégration continue fonctionne. Après une introduction sur ces éléments, nous verrons le principal outil pour la gestion des conteneurs logiciels (Docker) puis un outil très répandu pour l’intégration continue (Jenkins). Nous évoquerons ensuite par évoquer la mise en production avec la problématique du déploiement continu. Nous finirons par une comparaison des outils d’intégration continue.

            .. event:: Docker [2/3]
                :begin: 14:00
                :end: 18:00
                :speaker: Johan Moreau (IRCAD) et Alexandre Ancel (IHU Strasbourg)


        .. day:: 30-05-2017

            .. event:: Docker [3/3]
                :begin: 09:00
                :end: 10:00
                :speaker: Johan Moreau (IRCAD)


            .. event:: Jenkins [1/2]
                :begin: 09:00
                :end: 10:00
                :speaker: Johan Moreau (IRCAD)
                :support: attachments/spip/IMG/pdf/170518_rd_ci-2.pdf

                Depuis quelques années, les méthodes agiles ont fait évoluer la manière de réaliser des logiciels. Entre les valeurs (communication, simplicité, …) et les pratiques (petites livraisons, tests, …) de ces méthodes, l’intégration continue joue un rôle fondamental dans ces processus. Se basant sur la possibilité d’accéder au code à tout moment et la possibilité de construire l’application de manière automatique, cet outil devient le chef d’orchestre de la cadence de développement. De nombreux outils existent depuis quelques années, mais un changement récent dans les infrastructures informatiques redistribuent profondément les cartes. En effet l’arrivée des conteneurs logiciels modifient la manière de créer des applications, avec la notion d’architecture micro-services, et donc par effet de bord, la manière donc l’intégration continue fonctionne. Après une introduction sur ces éléments, nous verrons le principal outil pour la gestion des conteneurs logiciels (Docker) puis un outil très répandu pour l’intégration continue (Jenkins). Nous évoquerons ensuite par évoquer la mise en production avec la problématique du déploiement continu. Nous finirons par une comparaison des outils d’intégration continue.


            .. event:: Jenkins [2/2]
                :begin: 14:00
                :end: 15:30
                :speaker: Johan Moreau (IRCAD)


            .. event:: GitLab CI [1/2]
                :begin: 15:45
                :end: 18:00
                :speaker: Benoît Bayol (CentraleSupélec)
                :support:
                    [TP](https://bayol.pages.math.unistra.fr/tp-gitlab)
                    [PDF](attachments/spip/IMG/pdf/gitlab_worflow.pdf)

                Pourquoi et comment faire de l'intégration continue dans un environnement scientifique à l'aide de gitlab ?

                L'intégration continue est une bonne pratique de développement logiciel permettant notamment d'améliorer la qualité des simulateurs et modèles numériques écrits collaborativement en automatisant les passages de tests unitaires, d'intégration et de non-régression. C'est une étape importante à maîtriser afin de tendre vers une meilleure reproductibilité des expériences virtuelles en limitant l'introduction de régression notamment.

                Gitlab est un logiciel open-source qui a vu le jour en 2011 et qui possède des fonctionnalités permettant de faire de l'intégration continue au moyen d'un simple fichier ".gitlab-ci.yml" à la racine de votre dépôt git. Ce fichier contient des instructions permettant d'effectuer des tâches sur des serveurs distants ("runner") afin de construire le logiciel et de le tester.

                Dans cet atelier nous verrons donc :

                - le flux de développement général au moyen de gitlab dans le cadre de l'intégration continue
                - les bases de l'intégration continue au moyen du fichier ".gitlab-ci.yml"
                - l'utilisation d'un "runner partagé" et d'un "runner privé" ainsi que les questions de sécurité que cela pose.
                - l'utilisation de Docker sur les "runners" afin d'avoir des environnements de tests distincts


        .. day:: 31-05-2017

            .. event:: GitLab CI [2/2]
                :begin: 09:00
                :end: 10:45
                :speaker: Benoît Bayol (CentraleSupélec)


            .. event:: Travis CI [1/2]
                :begin: 11:00
                :end: 12:30
                :speaker: Loïc Estève (Inria)
                :support: https://github.com/lesteve/travis-tutorial

                `Travis <https://travis-ci.org/>`__ est une plateforme d'intégration continue utilisée par un grand nombre de projets open-source.

                Après une court exposé sur comment configurer Travis, la séance sera en majeure partie un TP qui couvrira :

                - matrices de build
                - cache Travis
                - utilisation de docker

                Prérequis à installer :

                - Git : pas de version minimale nécessaire. Git est natif ou très bien empaqueté pour Linux et Mac. Pour Windows, `ce projet <https://git-for-windows.github.io/>`__ devrait vous permettre de l'installer
                - `Docker <https://www.docker.com/community-edition#/download>`__ : si possible la version 1.13, voire 17.03-ce.
                - `Instructions pour Travis-CI  <https://github.com/lesteve/travis-tutorial>`__


            .. event:: Travis CI [2/2]
                :begin: 14:00
                :end: 16:30
                :speaker: Loïc Estève (Inria)


            .. event:: Synthèse sur les outils
                :begin: 16:30
                :end: 17:00

                `Synthèse <https://atelier-ci.pages.math.unistra.fr/synthese/>`__


.. section:: Organisation
    :class: orga

    - Matthieu Boileau (IRMA/CNRS)
    - Loic Gouarin (LMO/CNRS)
    - Pierre Navaro (IRMAR/CNRS)
