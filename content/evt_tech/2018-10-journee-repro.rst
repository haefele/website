Campagnes de calcul reproductibles
##################################

:date: 2018-06-15 15:49:15
:category: journee
:start_date: 2018-10-23
:end_date: 2018-10-23
:place: Lyon

.. contents::

.. section:: Description
    :class: description

    L’objectif de cette journée est de découvrir l'exploitation d'outils permettant de **réaliser des campagnes de calcul reproductibles**  pour des applications de modélisation, simulations paramétriques, benchmarking, analyses de données numériques ou expérimentales.

.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 23-10-2018

            .. event:: Reproductibilité
                :speaker: Anne Cadiou
                :begin: 09:00
                :end: 10:00
                :support: attachments/spip/IMG/pdf/2018-reproducibility.pdf

            .. event:: Execo
                :speaker: Matthieu Imbert, Laurent Pouilloux
                :begin: 10:00
                :end: 12:00
                :support: attachments/spip/IMG/pdf/2018-ExecoExpeNum.pdf

            .. break_event:: Buffet
                :begin: 12:00
                :end: 14:00

            .. event:: Openmole
                :speaker: Romain Reuillon, Mathieu Leclaire
                :begin: 14:00
                :end: 16:00
                :support: attachments/spip/IMG/tar/openmole.tar

            .. event:: Enjeux pour le HPC et discussion
                :speaker: Anne Cadiou
                :begin: 16:00
                :end: 17:00


.. section:: Organisation
    :class: orga

        - Laurent Pouilloux
        - Anne Cadiou
        - Simon Delamare

.. section:: Partenariat
   :class: orga

   - Mission pour l'Interdisciplinarité du CNRS
   - DR15 CNRS
