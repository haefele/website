Hackathon |_| machines de calcul
################################

:date: 2019-06-13
:category: journee
:tags: clusters, hpc, administration, mesocentres
:start_date: 2019-09-24
:end_date: 2019-09-25
:place: IHP
:summary: Ce hackathon réunit des administrateurs de clusters et a pour but de travailler et proposer des solutions sur des sujets tels que la configuration d'un gestionnaire de tâches, l'installation de bibliothèques scientifiques optimisées, la configuration d'un système de fichiers distribué, voire la préparation d'un ensemble de benchmarks pour tester la bonne santé du système et de l'infrastructure.
:inscription_link: https://indico.mathrice.fr/event/182/overview


.. contents::

.. section:: Description
    :class: description

    Ce hackathon a pour but de travailler et proposer des solutions sur des sujets tels que la configuration d'un gestionnaire de tâches, l'installation de bibliothèques scientifiques optimisées, la configuration d'un système de fichiers distribué, voire la préparation d'un ensemble de benchmarks pour tester la bonne santé du système et de l'infrastructure.  Les sujets seront proposés et choisis par les participants.  
    
    À l'issue du hackathon, une restitution sera présentée aux JCAD organisées à Toulouse du 9 au 11 octobre, et les solutions aux problèmes résolus pendant l'événement seront mises à disposition de la communauté, sous forme de scripts, de documentation, de codes, selon le type de problème.  
    
    Le hackathon s'adresse à tous les administrateurs de machines de calcul, allant de la machine de laboratoire au supercalculateur d'un centre de calcul.

.. section:: Organisation
    :class: orga

    - Richard Andriatoamanana
    - Fabrice Roy
