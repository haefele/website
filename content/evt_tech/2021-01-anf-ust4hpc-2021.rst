ANF User Support Tools for HPC 2021
###################################

:date: 2021-01-18
:category: formation
:tags: hpc, reproductibilté, cloud
:start_date: 2021-01-18
:end_date: 2021-01-26
:place: En ligne
:summary: Action Nationale de Formation du CNRS portée par le groupe Calcul et Resinfo


.. contents::

.. section:: Description
    :class: description

    .. section:: Contexte

        Les besoins en calcul numérique ne cessent de progresser.
        Le calcul intensif ne concerne plus seulement les applications traditionnelles (climat, astrophysique, mécanique des fluides, etc.) mais voit la montée en puissance d'autres disciplines (modélisation du vivant, nanotechnologies, etc.) et l'émergence de nouvelles communautés (santé, SHS).
        Compte tenu de l'évolution des technologies et de leur coût, la puissance de calcul déployée, en particulier au sein d'infrastructures mutualisées comme les mésocentres, est amenée à s'amplifier encore dans les années à venir. L'arrivée des accélérateurs complexifie les architectures en introduisant la notion de supercalcul hétérogène.
        Ainsi, les administrateurs et les utilisateurs des plateformes HPC sont confrontés à de nouvelles problématiques à la fois techniques, scientifiques et administratives. Cette évolution s'accompagne d'une multiplication des outils (middleware, compilateurs, applicatifs, etc.) qui rend encore plus complexe leur mise à disposition sur les machines de calcul pour les utilisateurs.


    .. section:: Enjeux

        Ces dernières années ont vu l'explosion de techniques destinées à faciliter le portage, la maintenance et les mises à jour des applications des utilisateurs.
        Ainsi, l'avènement de nouveaux types de systèmes de paquets ou des conteneurs d'application ouvrent des perspectives intéressantes pour le déploiement, mais aussi la reproductibilité des installations de logiciels.
        Ces nouveaux outils sont devenus incontournables pour l'administration système de machines de calcul. Ce sont aussi des avancées importantes pour les utilisateurs.
        D'une part, ils permettent de porter facilement les codes sur l'échelle de la pyramide du calcul : de l'ordinateur portable aux machines de niveau européen.
        D'autre part, ils fonctionnent particulièrement bien avec les systèmes d'intégration continue qui sont devenus incontournables en génie logiciel.
        Enfin, ils s'accompagnent d'avancées dans d'autres domaines connexes comme le stockage ou les ordonnanceurs de tâches.

    .. section:: Objectif

        L'objectif de cette action nationale de formation (ANF) est de poursuivre les actions d'exploration et de mise à jour des connaissances qui ont été initiées lors de la première session de `UST4HPC en 2018 <https://ust4hpc.sciencesconf.org/>`_.
        Cette année, le fil rouge sera le thème de la reproductibilité dans le contexte HPC.
        Nous visons en particulier l'utilisation des outils de packaging, des conteneurs et des notebooks Jupyter pour le HPC, les questions de sécurité et les systèmes d'intégration continue, le tout dans un contexte de calcul intensif.
        À noter que la participation à l'ANF 2018 n'est pas un pré-requis pour l'ANF 2021.

        Les 4 grands axes du programme de formation sont :

        - la reproductibilité, le packaging et les problématiques de sécurité associées
        - les conteneurs et runtimes pour le HPC
        - les notebooks jupyter dans un environnement HPC
        - l'intégration continue dans le contexte HPC

        Le `programme <#programme>`_ est encore susceptible d'être modifié.

    .. section:: Public visé

        Cette ANF s'adresse aux personnes en charge de l'administration de moyens de calcul mutualisés.

    .. section:: Modalité

        En raison des conditions sanitaires, la formation se fera intégralement à distance par le biais d'outils de webconférence et de discussion instantanée.

    .. section:: Inscription

        Le nombre de places est limité.
        En raison d'un grand nombre de candidatures, nous avons fermé prématurément les demandes d'inscriptions.

    Nombre de participants : 28

.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 18-01-2021
                
                .. event:: Accueil
                    :begin: 09:00
                    :end: 10:00
                    :speaker: Matthieu Boileau, Violaine Louvet, David Delavennat
        
                    
                    Tour de table des participants
                
                .. event:: Introduction sur les principes généraux de la reproductibilité dans le domaine du calcul
                    :begin: 10:00
                    :end: 11:00
                    :speaker: Konrad Hinsen
                    :support:
                        [Diaporama](attachments/evt/2021-01-anf-ust4hpc-2021/support00.pdf)
                        [Vidéo](https://www.canal-u.tv/video/groupe_calcul/introduction_sur_les_principes_generaux_de_la_reproductibilite_dans_le_domaine_du_calcul.59563)
        
                    
                    Sujets: Le rôle de la reproductibilité dans la recherche scientifique. La crise de la reproductibilité. La différence entre reproductibilité et réplicabilité. L'importance de la reproductibilité comme socle technique pour explorer la réplicabilité. La reproductibilité du calcul déterministe. L'importance de l'environnement computationnel. Les difficultés pratiques dans la préservation et documentation des environnements. La particularité de l'arithmétique à virgule flottante. La reproductibilité du calcul parallèle.
                
                .. break_event:: Pause café
                    :begin: 11:00
                    :end: 11:15
        
                
                
                .. event:: Au-delà des conteneurs : environnements reproductibles avec GNU Guix
                    :begin: 11:15
                    :end: 12:15
                    :speaker: Ludovic Courtès
                    :support:
                        [Diaporama](attachments/evt/2021-01-anf-ust4hpc-2021/support01.pdf)
                        [Vidéo](https://www.canal-u.tv/video/groupe_calcul/au_dela_des_conteneurs_environnements_reproductibles_avec_gnu_guix.59571)
        
                    
                    Cet exposé part de deux constats : la nécessité de déploiements logiciels reproductibles en support de la recherche reproductible et les limitations des outils les plus répandus.  Les « gestionnaires de paquets » utilisés en HPC, tels que Spack ou EasyBuild, offrent une grande flexibilité mais ne permettent pas de reproduire un environnement logiciel à l’identique ; les outils de « conteneurs », comme Docker ou Singularity, permettent de sauvegarder un environnement logiciel aux prix de l’opacité et de la rigidité d’un gros binaire.
                    
                    GNU Guix cherche à combiner les avantages de ces deux approches.  Cet exposé donne un aperçu des cas d’usages de Guix et de la manière dont il permet de reproduire des environnement logiciels au bit près, sur différentes machines et à différents instants, tout en conservant une traçabilité complète et la flexibilité des outils des gestion de paquets.  Parmi les développements Guix intéressant la recherche reproductible et le HPC, nous verrons « guix time-machine », l’intégration avec Software Heritage, mais aussi les facilités d’administration système pour une grappe de calcul.
        .. day:: 19-01-2021
                
                .. event:: Prise en main de GNU Guix — TP
                    :begin: 09:00
                    :end: 10:30
                    :speaker: Ludovic Courtès, Hinsen Konrad
                    :support:
                        [Notes orgmode](attachments/evt/2021-01-anf-ust4hpc-2021/support02.org)
        
                    
                    Ces travaux pratiques ont pour objectif de démarrer avec GNU Guix : installation de paquets, gestion des variables d’environnement, création d’environnements d’exécution isolés, déclaration d’environnements avec des « manifestes » et « voyage dans le temps » avec « guix time-machine ».
                
                .. break_event:: Pause café
                    :begin: 10:30
                    :end: 11:00
        
                
                
                .. event:: Packaging dans un centre de Tier 1
                    :begin: 11:00
                    :end: 12:00
                    :speaker: Rémi Lacroix
                    :support:
                        [Diaporama](attachments/evt/2021-01-anf-ust4hpc-2021/support03.pdf)
                        [Vidéo](https://www.canal-u.tv/video/groupe_calcul/packaging_dans_un_centre_de_tier_1.59773)
        
                    
                    
        .. day:: 20-01-2021
                
                .. event:: Conteneurs et runtime pour le HPC
                    :begin: 09:00
                    :end: 10:00
                    :speaker: Martin Souchal
                    :support:
                        [Version HTML](https://souchal.pages.in2p3.fr/container4science/prez.html)
                        [Version PDF](attachments/evt/2021-01-anf-ust4hpc-2021/support04.pdf)
                        [Vidéo](https://www.canal-u.tv/video/groupe_calcul/conteneurs_et_runtime_pour_le_hpc.59733)
        
                    
                    
                
                .. break_event:: Pause café
                    :begin: 10:00
                    :end: 10:15
        
                
                
                .. event:: TP singularity
                    :begin: 10:15
                    :end: 12:15
                    :speaker: Sébastien Gadrat, Martin Souchal
                    :support:
                        [Projet GitLab](https://gitlab.in2p3.fr/souchal/tp-singularty-v3)
        
                    
                    
        .. day:: 21-01-2021
                
                .. event:: FG-Cloud : un Cloud fédéré pluridisciplinaire pour le déploiement et l'orchestration de conteneurs
                    :begin: 09:00
                    :end: 10:00
                    :speaker: Jerome Pansanel
                    :support:
                        [Diaporama](attachments/evt/2021-01-anf-ust4hpc-2021/support05.pdf)
                        [Vidéo](https://www.canal-u.tv/video/groupe_calcul/fg_cloud_un_cloud_federe_pluridisciplinaire_pour_le_deploiement_et_l_orchestration_de_conteneurs.59777)
        
                    
                    
                
                .. event:: Conteneurs dans un centre de Tier 1
                    :begin: 10:10
                    :end: 10:30
                    :speaker: Rémi Lacroix
                    :support:
                        [Diaporama](attachments/evt/2021-01-anf-ust4hpc-2021/support06.pdf)
                        [Vidéo](https://www.canal-u.tv/video/groupe_calcul/conteneurs_dans_un_centre_de_tier_1.59775)
        
                    
                    
                
                .. break_event:: Café
                    :begin: 10:30
                    :end: 11:00
        
                
                
                .. event:: Déploiement d'un jupyterhub avec kubernetes - exposé
                    :begin: 11:00
                    :end: 12:00
                    :speaker: Rémi Cailletaud
                    :support:
                        [Diaporama](attachments/evt/2021-01-anf-ust4hpc-2021/support07.pdf)
                        [Vidéo](https://www.canal-u.tv/video/groupe_calcul/deploiement_d_un_jupyterhub_avec_kubernetes_expose.59737)
        
                    
                    
        .. day:: 22-01-2021
                
                .. event:: Déploiement d'un jupyterhub avec kubernetes - tutoriel
                    :begin: 09:15
                    :end: 10:30
                    :speaker: Rémi Cailletaud
                    :support:
                        [Projet GitLab](https://gricad-gitlab.univ-grenoble-alpes.fr/ust4hpc/gpu-hub)
                        [Vidéo](https://www.canal-u.tv/video/groupe_calcul/deploiement_d_un_jupyterhub_avec_kubernetes_demonstration.59739)
        
                    
                    
                
                .. break_event:: Café
                    :begin: 10:30
                    :end: 11:00
        
                
                
                .. event:: Déploiement d'un jupyterhub avec kubernetes - tutoriel - suite
                    :begin: 11:00
                    :end: 12:00
                    :speaker: Rémi Cailletaud
        
                    
                    
        .. day:: 25-01-2021
                
                .. event:: Debriefing tutoriel jupyterhub avec kubernetes
                    :begin: 09:00
                    :end: 09:30
                    :speaker: Rémi Cailletaud
        
                    
                    
                
                .. event:: Vers un environnement reproductible pour les bloc-notes Jupyter
                    :begin: 09:30
                    :end: 10:00
                    :speaker: Ludovic Courtès
                    :support:
                        [Diaporama](attachments/evt/2021-01-anf-ust4hpc-2021/support08.pdf)
                        [Vidéo](https://www.canal-u.tv/video/groupe_calcul/vers_un_environnement_reproductible_pour_les_bloc_notes_jupyter.59745)
        
                    
                    Les bloc-notes Jupyter sont devenus un élément important dans la boîte à outils de la recherche reproductible, permettant de mêler un récit scientifique à du code et au résultat de son évaluation.  Ces bloc-notes souffrent toutefois d’un problème majeur : ils ne déclarent pas l’environnement logiciel dans lequel doit s’exécuter le code.
                    
                    Cet exposé présente les limites des solutions les plus courantes à ce problème et propose une approche différente.  Guix-Jupyter est un « noyau » Jupyter qui permet d’include dans les bloc-notes des annotations de déploiement logiciel mais aussi de dépendance sur des données externes.  Guix-Jupyter en est à ses débuts mais il montre déjà que des bloc-notes auto-suffisants et reproductibles sont possibles.
                
                .. event:: Prise en main de Guix-Jupyter — TP
                    :begin: 10:00
                    :end: 11:00
                    :speaker: Ludovic Courtès
                    :support:
                        [Notes orgmode](attachments/evt/2021-01-anf-ust4hpc-2021/support09.org)
        
                    
                    
                
                .. break_event:: Café
                    :begin: 11:00
                    :end: 11:20
        
                
                
                .. event:: Notebook Jupyter dans un centre de Tier 1
                    :begin: 11:20
                    :end: 11:40
                    :speaker: Rémi Lacroix
                    :support:
                        [Diaporama](attachments/evt/2021-01-anf-ust4hpc-2021/support10.pdf)
                        [Vidéo](https://www.canal-u.tv/video/groupe_calcul/packaging_dans_un_centre_de_tier_1.59773)
        
                    
                    
        .. day:: 26-01-2021
                
                .. event:: Intégration continue pour le HPC sur PlaFRIM
                    :begin: 09:00
                    :end: 10:00
                    :speaker: François Rué
                    :support:
                        [Diaporama](attachments/evt/2021-01-anf-ust4hpc-2021/support11.pdf)
                        [Vidéo](https://www.canal-u.tv/video/groupe_calcul/integration_continue_pour_le_hpc_sur_plafrim.59779)
        
                    
                    L'objectif, à travers cette présentation, est d'exposer l'état de l'expérience menée à travers différents centres de partages de ressources. Ce partage concerne en particuliers les responsables de plateforme outil pour l'nitégration continue d'un côté et les administrateurs de moyens de calcul de l'autre, mettant à disposition de la communauté des chercheurs en HPC des plateformes dédiées pour l'intégration continue dans ce contexte particuliers. 
                
                .. event:: Utilisation de PlaFRIM pour l’intégration continue des codes d’algèbre
                    :begin: 10:00
                    :end: 11:00
                    :speaker: Florent Pruvost
                    :support:
                        [Diaporama](attachments/evt/2021-01-anf-ust4hpc-2021/support12.pdf)
                        [Vidéo](https://www.canal-u.tv/video/groupe_calcul/utilisation_de_plafrim_pour_l_integration_continue_des_codes_d_algebre.59747)
        
                    
                    L'équipe de recherche Inria HiePACS développe une pile logicielle de solveurs d'algèbre linéaire parallèles. 
                    Les enjeux majeurs sont la performance et le passage à l'échelle sur des machines de calculs toujours plus grandes et hétérogènes (CPUs Intel, AMD, IBM, GPUs Nvidia, ...).
                    Pour répondre à ce challenge ces solveurs numériques tirent partis de nombreuses autres bibliothèques sous-jacentes spécialisées (moteurs d'exécutions, noyaux de calcul, MPI, CUDA, ...) et deviennent des objets logiciels modulaires et complexes.
                    La compréhension, le contrôle et la répétabilité des performances obtenues est aussi difficile car dépend de nombreux paramètres : architecture des noeuds, réseaux, versions des bibliothèques, paramètres d'entrées.
                    Afin de mieux maîtriser l'évolution des performances au cours des développements nous avons mis au point une procédure automatisée qui nous permet de suivre nos résultats en fonction des commits des solveurs tout en contrôlant l'ensemble de paramètres qui influent sur ces résultats, rendant la procédure reproductible d'un point de vue logiciel.
                    Nous détaillerons dans cette présentation la procédure, les outils utilisés et discuterons des limitations actuelles et de quelques perspectives.
                
                .. event:: Déclusion
                    :begin: 11:00
                    :end: 12:00
                    :speaker: David Delavennat, Matthieu Boileau, Violaine Louvet, Fabrice Roy
        
                    
                    Retour sur la formation et réflexion sur les thèmes de la prochaine ANF


.. section:: Partenaires
    :class: description

    .. container:: text-align-center
        
        .. image:: attachments/logo-partenaires/cnrs.png
            :alt: Logo du CNRS
            :target: https://www.cnrs.fr
            :height: 100px

        .. image:: attachments/logo/Logo_small_noir.png
            :alt: Logo du Groupe Calcul
            :target: http://calcul.math.cnrs.fr/
            :height: 100px

        .. image:: attachments/logo-partenaires/logo_resinfo.png
            :alt: Logo de Resinfo
            :target: https://resinfo.org/
            :height: 50px


.. section:: Organisation
    :class: orga

    - Matthieu Boileau
    - David Delavennat
    - Violaine Louvet
    - Lucy Ruffier