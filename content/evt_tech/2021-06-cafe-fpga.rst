FPGA : un nouveau type d'accélérateur pour le calcul ?
######################################################

:date: 2021-06-24 10:00:00
:category: cafe
:tags: visio
:start_date: 2021-06-24 10:00:00
:end_date: 2021-06-24 11:00:00
:place: En ligne
:summary: Calcul scientifique sur FPGA
:inscription_link: https://indico.mathrice.fr/event/256
:calendar_link: https://indico.mathrice.fr/export/event/256.ics

.. contents::

.. section:: Description
    :class: description

    Pour ce Café, nous accueillons Matthieu Haefele

    Les Field Programmable Gate Arrays (FPGA) sont des puces électroniques qui se situent entre des ASICs, des puces dédiées à certains traitements, et des processeurs conventionnels (CPU). Cette notion provient de l'aspect reconfigurable de ces dispositifs, les rendant plus flexibles que des ASICs (au prix d'une surface utile de silicium plus petite et d'un besoin en énergie plus important) mais beaucoup plus efficaces en terme d'énergie que les CPU ou même les processeurs graphiques (GPU).

    Ils sont principalement utilisés dans le domaine de l'électronique embarquée avec des applications à faibles besoins énergétiques. Une productivité de développement faible, un manque de portabilté entre dispositifs et des temps de compilation très longs sont les contraintes fortes qui ont vraisemblablement empêché leur utilisation dans le domaine du HPC jusqu'ici. Mais l'apparition de modèles récents ayant de très grandes capacités, conjuguée avec des modèles de programmation de plus hauts niveaux, pourraient rendre leur utilisation possible dans les très grands calculateurs, aujourd'hui limités par le besoin en énergie électrique.

    Ce café propose de présenter ce nouveau type d'accélérateur et son utilisation dans le domaine du calcul avec l'implémentation de plusieurs noyaux de calcul issus d'un code de production de dynamique moléculaire en utilisant le modèle de programmation Maxeler MaxJ. Les résultats obtenus en terme de temps et d'énergie seront comparés aux implémentations CPU et GPU de ces mêmes noyaux.
    
    La présentation durera 30-40 minutes et sera suivie d'une séance de questions.

    Elle est accessible à tous et aura lieu sur la plateforme BBB de Mathrice. Merci de bien vouloir vous inscrire pour suivre cette session.
    

.. button:: Support de la présentation (HTML)
    :target: attachments/evt/cafes/2021-06_Cafe_Calcul_FPGA_html.tgz

.. button:: Support de la présentation (PDF)
    :target: attachments/evt/cafes/2021-06_Cafe_Calcul_FPGA.pdf

.. button:: Vidéo sur canal-u.tv
    :target: https://www.canal-u.tv/video/groupe_calcul/fpga_un_nouveau_type_d_accelerateur_pour_le_calcul.62273

.. section:: Orateur
    :class: orateur

      - Matthieu Haefele (`Laboratoire de Mathématique et de ses Applications de Pau <https://lma-umr5142.univ-pau.fr/fr/index.html>`_)
      
