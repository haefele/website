Title: Ingénieur Calcul Scientifique
Date: 2021-03-12 16:57
Slug: job_17c03f4ffeedfa82649b93ce6fc20886
Category: job
Authors: Violette Thermes
Email: violette.thermes@inrae.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Rennes
Job_Duration: 1 an
Job_Website: https://www6.rennes.inrae.fr/lpgp/Actualites/Deux-nouveaux-projets-ANR-selectionnes-pour-le-laboratoire
Job_Employer: INRAE - LPGP
Expiration_Date: 2021-09-01
Attachment: job_17c03f4ffeedfa82649b93ce6fc20886_attachment.pdf

**Objectif de l’emploi** :
 
Modélisation, estimation de paramètres et simulation numérique pour comprendre l’ovogenèse chez le poisson.

**Contexte et objectifs du projet** :

Le projet DynaMO formalise une collaboration entre le LPGP et l’IRMAR, il bénéficie d’un financement ANR JCJC, 2019 - 2023.
La maîtrise de la quantité d’œufs produits à chaque cycle reproducteur (fécondité) est un enjeu important que ce soit pour la gestion des populations sauvages de poissons ou en tant que levier d’amélioration de la compétitivité des élevages aquacoles. La dynamique globale de l’ovogenèse (taux et fréquence de recrutement, puis de croissance des ovocytes au sein de l’ovaire) peut être extrêmement variable que ce soit entre les différentes espèces de poissons, qui présentent des fécondités très variables, mais également au sein d’une même espèce en fonction des conditions environnementales. Les mécanismes qui gouvernent la dynamique de recrutement/croissance des ovocytes durant l’ovogenèse restent toutefois à ce jour peu connus, essentiellement en raison de difficultés méthodologiques inhérente à l’histologie classique (2D sur coupes) qui ne permet pas d’accéder facilement à la totalité des ovocytes à l’échelle de l’ovaire entier.
Chez le médaka, un petit poisson d’aquarium à cycle de reproduction court utilisé pour étudier l’ovogenèse, nous avons récemment mis en évidence une vingtaine de miARNs exprimés de façon prédominante dans l’ovaire, dont un miARN (miR-202) jouant un rôle crucial pour le succès reproducteur femelle et notamment la fécondité. Les miARNs sont des régulateurs fins de l’expression des gènes qui sont impliqués dans le contrôle de nombreux processus biologiques et les données préliminaires dont nous disposons indiquent qu’il jouent un rôle important dans la régulation de la fécondité chez le médaka.

**Missions** :

- Un premier modèle hybride d’EDO a été élaboré et testé au cours d’un stage de Master de 2019 pour le médaka. Ce modèle est à améliorer en fonction des nouvelles données disponibles et à comparer avec d’autres types de modèle : EDP, individu centré.
- Des miARNs ont été identifiés et des expériences à partir de mutants sont programmées pour déterminer les fonctions de ces miARNs. Pour chacun, les paramètres du modèle seront estimés pour déterminer les différences avec ce qui a été obtenu pour les médakas sauvages et chercher à préciser la fonction et la zone d’action de ce miARN.
- Un dialogue constant avec les équipes impliquées sera nécessaire pour aider à l’interprétation des résultats, l’amélioration du modèle et éventuellement la programmation d’expérimentations complémentaires.
- Il est également attendue une participation à la publication scientifique dans les colloques et revues.

**Profil du candidat** :

Le candidat a une formation de niveau bac + 5 en calcul scientifique : Master ou ingénieur. Il aura des compétences en modélisation mathématique, analyse numérique, simulation numérique, programmation scientifique et un intérêt pour la biologie. Une aptitude au travail en équipe est nécessaire.
