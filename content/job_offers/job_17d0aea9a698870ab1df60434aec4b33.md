Title: Développement d&#39;une bibliothéque parallèle des tenseurs
Date: 2020-11-06 11:52
Slug: job_17d0aea9a698870ab1df60434aec4b33
Category: job
Authors: Laura Grigori
Email: laura.grigori@inria.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Inria Paris
Job_Duration: 1 an renouvelable
Job_Website: 
Job_Employer: Inria Paris
Expiration_Date: 2020-12-31
Attachment: 


An opening for a software engineer is available in the Inria Alpines group in Paris, Inria and Sorbonne University to work in the context of the EMC2 project funded by ERC. EMC2 is an interdisciplinary project between mathematicians, computer scientists and chemists, aiming at providing a new generation, dramatically faster and quantitatively reliable molecular simulation software.

In this context, the candidate will participate in the development of a highly parallel tensor library that will be used in quantum chemistry for solving problems in high dimensions on large scale
computers.  The contract is for one year (CDD) renewable.

Skills: Good background in C or C++ and MPI parallel coding. Good background in linear algebra or molecular simulations is a plus.

Websites:
EMC2 ERC project: <https://erc-emc2.eu/>
Alpines group: <https://team.inria.fr/alpines/>

