Title:  Chef de projet ou expert en ingénierie logicielle (H/F)
Date: 2020-06-08 14:07
Slug: job_3b95ea9a7e8561c1397e9374b5cc9a9b
Category: job
Authors: Geneviève Morvan
Email: communication@idris.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Orsay (91) - France
Job_Duration: 
Job_Website: http://www.idris.fr
Job_Employer: CNRS - IDRIS
Expiration_Date: 2020-07-02
Attachment: 

Dans le cadre des concours de recrutement externes CNRS 2020, l&#39;IDRIS recrute 
 
 **un chef de projet ou expert en Ingénierie logicielle (H/F),
 ingénieur de recherche , Concours 52**

Dans le cadre de la stratégie nationale de recherche en intelligence artificielle (IA) définie par le gouvernement en novembre 2018, l&#39;IDRIS opère la première plate-forme nationale pour l&#39;IA. Pour assurer cette nouvelle mission de responsabilité nationale, le CNRS recherche un(e) expert(e) en intelligence artificielle, pour intégrer l&#39;équipe en charge du support de l&#39;ensemble des projets de recherche de ce domaine qui utiliseront cette nouvelle plate-forme. 

Pour plus d&#39;informations sur les postes à pourvoir, consultez le site web de l&#39;IDRIS : http://www.idris.fr/annonces/idris-recrute.html