Title: Traitement de signaux neurophysiologiques en Python
Date: 2021-05-14 15:11
Slug: job_4aa7c3f1dbebf99b6292836cf863bef0
Category: job
Authors: Anne Cheylus
Email: anne.cheylus@inserm.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Bron
Job_Duration: 4 mois
Job_Website: https://www.crnl.fr/fr/offre/lequipe-cmo-recrute-ingenieur-cdd-compter-septembre-2021
Job_Employer: CRNL, laboratoire public financé par le CNRS, l’INSERM et l’Université de Lyon
Expiration_Date: 2021-09-01
Attachment: job_4aa7c3f1dbebf99b6292836cf863bef0_attachment.pdf

L’équipe **[Codage et Mémoire olfactive (CMO CRNL)](https://www.crnl.fr/fr/equipe/cmo)** recrute un ingénieur pour une première mission de 4 mois (éventuellement renouvelable) à compter du mois de Septembre.

Dans le cadre de son projet de recherche, notre équipe recueille des signaux neurophysiologiques cérébraux en lien avec des tâches comportementales de perception et de mémorisation impliquant des stimuli odorants, chez l’Homme comme chez l’animal.
Nous avons déjà développé certains outils d’analyse de ces signaux. La mission de l’ingénieur recruté sera :

1. D’optimiser leur implémentation et leur utilisation afin de faciliter et accélérer le traitement des données
2. D’intégrer d’autres analyses : étude de la cohérence, analyse des interactions cross-fréquentielles, etc).

La personne recrutée devra interagir d’une part avec les ingénieurs de l’équipe qui ont mis en place l’acquisition et le prétraitement des données, mais aussi avec les chercheurs qui les utilisent afin de répondre à des questions spécifiques.

En terme de compétences techniques, les prérequis sont :

- parfaite maîtrise de l&#39;écosystème &#34;Python scientifique&#34; (numpy / scipy / pandas / xarray / sklearn / jupyter /...)
- bonne maîtrise du système de gestion de versions et de configurations git
- bonnes notions de traitement de signal (filtrage, analyse spectrale, ...)
- notions générales en statistiques

Par ailleurs, nous serions particulièrement intéressés par les candidats qui auraient également :

- une expérience en analyse de données scientifiques biologiques
- une connaissance des outils de publication open-source et open-data

En terme de qualité humaine, nous recherchons quelqu’un qui aime travailler en équipe à l’interface entre les questions scientifiques et les outils pour les aborder.

Si ce profil est le vôtre rejoignez-nous vite \!

Si vous le souhaitez, vous pouvez nous contacter pour obtenir des informations supplémentaires et/ou envoyer vos candidatures à :
Samuel Garcia, [samuel.garcia@cnrs.fr ](mailto:samuel.garcia@cnrs.fr)(aspects techniques, programmation, analyse de signal)
Nadine Ravel, [nadine.ravel@cnrs.fr](mailto:nadine.ravel@cnrs.fr) (aspects scientifiques et biologiques du projet)

