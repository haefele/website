Title: Numerical study of the influence of symmetry on the melting of ice object in fluid flows
Date: 2020-11-24 21:19
Slug: job_544b218beb642ca843577435e2003ce6
Category: job
Authors: Agathe CHOUIPPE
Email: chouippe@unistra.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Strasbourg
Job_Duration: 5 à 6 mois
Job_Website: http://icube.unistra.fr/
Job_Employer: Laboratoire ICube
Expiration_Date: 2021-02-16
Attachment: job_544b218beb642ca843577435e2003ce6_attachment.pdf

This project aims at investigating, with the aid of numerical methods, the flow around solid bodies with deformable shapes. The melting of solids in liquids is relevant for many industrial and environmental applications and a special emphasis will be given here to the ice/water system, where the form modification is induced by ice melting. The goal of this masterthesis/internship is to shed more light on the influence of the shape of the object on the melting rate, focusing on solids with initially cylindrical or spherical shapes placed in uniform flows. The deformation of the ice/water interface is strongly coupled to the local structure of the flow and wake so that it is of great importance to understand how the modification of the ice form can affect the melting rate. For this reason a first part of the project will be devoted to the simulation of the flow around fixed-shape bodies featuring complex contours. The corresponding shapes will be chosen in accordance with the experimental observations of (Hao and Tao, 2002) and Ristroph et al. (2012). The second part of this work will deal with the simulation of the deformation of the interface and its coupling with the flow. The simulations will be performed with the aid of two numerical codes : simulation around fixed-shape bodies will use the spectral/spectral-element solver of Kotouč et al. (2008) while simulations accounting for the deformation will use the NSIBM solver Durrenberger (2015), both developed at ICube.