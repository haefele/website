Title: Description symbolique des domaines de calculs pour la résolution automatique des équations aux dérivées partielles
Date: 2019-11-28 14:56
Slug: job_564e9c4fb290344e022b9ea2a2ec9a6c
Category: job
Authors: Antoine Falaize
Email: antoine.falaize@univ-lr.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Poitiers ou La Rochelle (en fonction du candidat)
Job_Duration: 5 à 6 mois
Job_Website: 
Job_Employer: Laboratoires XLim (Poitiers) et LaSIE (La Rochelle)
Expiration_Date: 2020-02-20
Attachment: job_564e9c4fb290344e022b9ea2a2ec9a6c_attachment.pdf

Le processus de résolution numérique d’un problème de calcul scientifique peut être découpé selon trois niveaux hiérarchiques [1]:
i. définition formelle du problème mathématique,
ii. description formelle de la méthode de discrétisation employée,
iii. résolution numérique du problème traité.

Verticalement à cette structure, on peut superposer quatre types d’objets distincts :
a. le problème,
b. les opérateurs,
c. les variables,
d. les domaines de calcul.

De nombreuses librairies (e.g. PETSc, FreeFEM++, Fenics, OpenFoam, SymEngine) sont disponibles pour traiter séparément les points (i–iii) et/ou décrire les objets (a–d) indépendamment de (i–iii). Cependant, peu d’outils s’attachent à traiter l’ensemble (i–iii et a– d) de manière cohérente.

Pour répondre à ce besoin, un projet de développement a été initié dans l’équipe M2N du LaSIE. L’approche est d’utiliser la librairie Python de calcul symbolique Sympy pour les points (i–ii), puis de générer automatiquement le code PETSc de résolution (iii) pour la plateforme ciblée (en C ou FORTRAN). Une difficulté est de décrire symboliquement les domaines de calcul (d) dans un objet Python interfacé (via Sympy) avec le code M2N.

Cette dernière difficulté pourrait être levée par l’utilisation de la bibliothèque Jerboa [2] développée dans l’équipe IG d’XLIM et dédiée à la modélisation géométrique. Jerboa fournit à la fois un langage dédié et une bibliothèque logicielle permettant de prototyper des modeleurs géométriques basés sur la topologie. Une fois les objets géométriques spécifiés (dimension topologique, géométries, informations physiques, etc.), les opérations peuvent être définies graphiquement dans Jerboa à l’aide de règles. Un analyseur syntaxique vérifie automatiquement la préservation de la cohérence des objets lors des futurs transformations. Une fois les opérations ainsi définies, le modeleur peut être généré et directement exploité.

L’objectif de ce stage est l’utilisation de Jerboa pour développer un modeleur dédier à la résolution de problèmes et son intégration le code du M2N du LaSIE. Plus précisément, ce stage comprend :
1. la création d’un modeleur géométrique dédié en utilisant Jerboa, qui permet la
représentation de domaines de calculs (d) simples en 2D et 3D (et donc leur construction) et leur exploitation pour inspecter un domaine (calculer ses frontières, les normales à une frontière, etc.) ou le discrétiser (le subdiviser pour construire un maillage),
2. la création d’une interface Python pour le modeleur C++ ci-dessus (via e.g. la librairie pybind11),
3. l’exploitation du modeleur géométrique ainsi obtenu pour décrire des domaines de calcul simples (typiquement un cube) et les exploiter dans le code M2N pour la description symbolique dans les étapes formelles (i-ii) et le maillage dans l’étape numérique (iii),
4. l’application à un modèle jouet à discuter avec le candidat (e.g. l’équation de la chaleur instationnaire avec conditions limites mixtes Dirichlet/Neumann).

Références :
[1] Labbé, S., Laminie, J., &amp; Louvet, V. (2003). Méthodologie et environnement de développement orientés objets : de l’analyse mathématique à la pro- grammation. Matapli, 70, 79-92.
[2] H. Belhaouari, A. Arnould, P. Le Gall et T. Bellet, « JERBOA : A Graph Transformation Library for Topology-Based Geometric Modeling », 7th International Conference on Graph Transformation, Springer Verlag LNCS 8571, 2014.