Title: Ingénieur en calcul scientifique haute performance
Date: 2020-01-24 12:57
Slug: job_621bb867f8963a0ea7f414571bebe765
Category: job
Authors: Edouard Audit
Email: edouard.audit@cea.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Maison de la Simulation (CEA, Saclay) / IDRIS
Job_Duration: 2 years
Job_Website: http://www.maisondelasimulation.fr/emploi/poste-dingenieur-specialiste-du-calcul-haute-performance-pour-une-duree-de-2-ans-renouvelable/
Job_Employer: Maison de la Simulation / IDRIS
Expiration_Date: 2020-05-01
Attachment: job_621bb867f8963a0ea7f414571bebe765_attachment.pdf

Dans le cadre du projet PRACE, la Maison de la Simulation propose un poste d’ingénieur spécialiste du calcul haute performance.

Il ou elle sera en charge de participer à l’expertise applicative pour les utilisateurs des machines Tier-0 de l’infrastructure PRACE. Après une première phase d’instrumentation des codes cibles à l’aide d’outils d’analyse de performance adaptés à l’exascale, vous pourrez être amené à réécrire, en collaboration avec les scientifiques développeurs des codes, tout ou partie de ces applications avec de nouveaux modèles de programmation ou langages afin de les adapter aux spécificités des architectures Tier-0 massivement parallèles et éventuellement hybrides accélérées de PRACE. Ce travail s’effectuera au sein des équipes de la Maison de la Simulation et de l’IDRIS, spécialistes du calcul haute performances. La personne sélectionnée rejoindra le HLST (High Level Support Team) Français piloté par un ingénieur de l’IDRIS.

### Compétences

* Thèse ou diplôme d’ingénieur dans un domaine scientifique fortement connexe au calcul intensif ;
* Connaissances opérationnelles des techniques et langages de programmation (Fortran90, C ou C++) et du développement de codes applicatifs ;
* Bonne expérience dans la parallélisation (MPI, OpenMP) et l’optimisation de codes scientifiques sur
diverses architectures (SMP, MPP) dans un environnement Unix ;
* Maîtrise de l’anglais technique à l’écrit et à l’oral (collaboration avec des chercheurs Européens)
* Aptitude à travailler en équipe.

En complément, des connaissances ou une expérience dans un ou plusieurs des domaines suivants seront appréciées :

* Utilisation d’outils d’analyse de performance et de débogage sur des applications parallèles ;
* Expérience du calcul sur accélérateur (GPU) ;
* Connaissance de l’architecture des ordinateurs ;
* Connaissance de langages de script (Python,…) ;

### Localisation

La Maison de la Simulation est située sur le plateau de Saclay et bi-localisée dans le bâtiment de l’IDRIS et dans le bâtiment Digitéo du site du CEA/Saclay.

Ce poste est un CDD du CEA (2 ans, renouvelable) proposant des conditions de travail et un salaire attractifs.

### Procédure de candidature

Envoyer vos candidatures (CV, lettre de motivation et références) ainsi que vos demandes d’information complémentaire à [info@maisondelasimulation.fr](mailto:info@maisondelasimulation.fr)

