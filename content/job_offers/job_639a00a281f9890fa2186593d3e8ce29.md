Title: Evaluation des notebooks Jupyter pour la gestion des fiches de validation de la plateforme open-source thermohydraulique TRUST
Date: 2020-10-26 10:00
Slug: job_639a00a281f9890fa2186593d3e8ce29
Category: job
Authors: Adrien Bruneton
Email: adrien.bruneton@cea.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: CEA Saclay
Job_Duration: 6 mois
Job_Website: 
Job_Employer: CEA Saclay
Expiration_Date: 2021-01-18
Attachment: job_639a00a281f9890fa2186593d3e8ce29_attachment.pdf

**Contexte**
La plateforme open-source thermohydraulique TRUST, développée au sein du CEA/DES, permet la résolution des équations de Navier-Stokes incompressibles.
L&#39;analyse des résultats de ce code de CFD se fait traditionnellement de plusieurs manières : 

- soit via la production de fichiers dédiés qui peuvent être exploités avec les logiciels de visualisation scientifiques ParaView et/ou VisIt, 
- soit via la production de fichier ASCII correspondant à des listings de valeurs de certaines variables échantillonnées en certains points (sondes).

 L&#39;ensemble de ces résultats peut être mis en forme au sein d&#39;un document unique. La trame de ce document est rédigée par l&#39;utilisateur en utilisant une syntaxe texte simplifiée dans laquelle du texte, des formules LaTex, des figures et des courbes extraites d&#39;un (ou plusieurs) run(s) du code sont insérées. L&#39;ensemble est assemblé automatiquement par un outil ad-hoc sous forme d&#39;un document LaTex finalement compilé en un document PDF. 
Ce type de document, nommé fiche de validation, sert à la fois d&#39;outil de présentation des résultats pour le physicien, mais est aussi utilisé dans les tests de non-régression du code lors des sorties de version du code. La plateforme dispose effectivement d&#39;outils de comparaison de fichiers PDF permettant de détecter les variations dans ces fiches.
Objectifs
Le but de ce stage est de revoir cette logique ad-hoc de mise en forme, de génération et de comparaison des fiches de validation, pour évaluer les capacités d&#39;autres outils standards et largement répandus, tels que les notebooks Jupyter. La pertinence du développement d&#39;un kernel TRUST pour Jupyter sera notamment évaluée (i.e. la possibilité de lancer un calcul TRUST depuis un notebook Jupyter).
De nombreux cas d&#39;applications concrets sur des calculs CFD turbulents serviront d&#39;illustration à la démarche et permettront aussi au stagiaire d&#39;en apprendre plus sur ce domaine, notamment sur la modélisation des écoulements turbulents.
 
**Environnement de travail**
Le stage s’effectuera au sein du Laboratoire de Génie Logiciel (ISAS/DES/DM2S/STMF/LGLS) sur le site du CEA à Saclay.

**Compétences requises ou souhaitées**

- scripting Python, shell et une aisance avec l&#39;environnement Unix en général,
- notions de base sur les notebooks Jupyter,
- des connaissances de base en mécanique des fluides sont souhaitables

**Profil recherché**
Formation Master 2 ou équivalent en informatique, maths applis, ou génie logiciel.
Mots-clés
TRUST, Jupyter, Python, validation.
