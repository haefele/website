Title: Ingénieur Système et Stockage HPC
Date: 2019-10-08 14:54
Slug: job_64851ebd5777858aca45d4236ff22f10
Category: job
Authors: MORTIER
Email: sylvain.mortier@atos.net
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Bruyères le Châtel (91)
Job_Duration: 
Job_Website: https://atos.net/fr/
Job_Employer: Atos
Expiration_Date: 2020-12-31
Attachment: 

Bonjour, nous recrutons un Ingénieur Système &amp; Stockage HPC avec des compétences Lustre pour travailler sur les supercalculateurs les plus avancés d&#39;Europe au sein de notre équipe basée à Ter@tec en Essonne.

Le descriptif de poste complet : https://career5.successfactors.eu/sfcareer/jobreqcareer?jobId=114829&amp;company=Atos&amp;username=

