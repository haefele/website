Title: Développment de la bibliothèque XIOS : Ingénieur (H/F) en développement d&#39;applications scientifiques / calcul intensif (HPC), dans le milieu de la recherche en modélisation du climat
Date: 2020-11-01 21:37
Slug: job_656f24c1d1880e51ec79bfbfb6dc6ebd
Category: job
Authors: Yann Meurdesoif
Email: yann.meurdesoif@cea.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: CEA/LSCE (Laboratoire des sciences du climat et de l&#39;environnement), plateau de Saclay
Job_Duration: 24 mois
Job_Website: https://emploi.cnrs.fr/Offres/CDD/FR636-EVEMAG-032/Default.aspx
Job_Employer: CNRS
Expiration_Date: 2020-11-23
Attachment: 

#### Contexte

Les modèles de simulation du système climatique produisent un important volume de données au cours de simulations qui sont réalisées sur des calculateurs pouvant compter jusqu’à 100 000 cœurs de calcul. A cette échelle, les écritures sur disques et le post-traitement des données, deviennent alors un important goulet d’étranglement. Afin de générer efficacement et exploiter de manière performante l’énorme flux de données généré par ses simulations, l’IPSL (Institut Pierre Simon Laplace)  a développé la bibliothèque XIOS. 

Cette bibliothèque, dédiée au calcul intensif, permet de gérer efficacement et simplement les entrée/sortie parallèles des données sur les systèmes de stockage. Dans cette nouvelle approche, orientée client/serveur, des cœurs de calcul sont exclusivement dédiés aux I/O de façon à minimiser leur impact sur le temps de calcul des modèles. L’utilisation des communications asynchrones entre les modèles (clients) et les serveurs I/O permet de lisser les pics I/O en envoyant un flux de données constant au système de fichiers tout au long de la simulation, recouvrant ainsi totalement les écritures par du calcul. 
Par ailleurs, XIOS implémente d’une part des fonctionnalités de workflow parallèle permettant d’effectuer le post-traitement des données « In Situ », c’est-à-dire en parallèle et sur l’ensemble des cœurs de calcul d’une simulation. Ce système de workflow, décrit la chaine de traitement allant de l’extraction des données en provenance des modèles (flux d’entrée), le chainage des opérateurs transformant et combinant ces flux et leur sortie dans les différents fichiers associés. Cette description s’effectue de manière souple à partir d’un fichier d’entrée XML (analysé au moment de l’exécution), déchargeant les modèles de cette tâche. Ce fichier XML est structuré hiérarchiquement et implémente des concepts d&#39;héritages parent-enfants, amenant à des définitions de la chaine de traitement extrêmement souples et compactes.

La bibliothèque XIOS est écrite en C++ (~120 000 lignes de codes) et utilise la bibliothèque MPI pour ses communications asynchrones entre clients et serveurs, ainsi que pour le traitement parallèle du workflow. Afin d&#39;agréger la bande passante du système de fichiers parallèle des calculateurs, XIOS fait appel aux I/O parallèles à travers les bibliothèques NETCDF4, HDF5 et MPI-IO. Aujourd&#39;hui, XIOS est utilisé par différents instituts en France et en Europe, et sa base d&#39;utilisateurs est en constante progression.
Situation de l’emploi et conditions :
La mission se déroulera au LSCE (Laboratoire des sciences du climat et de l’environnement) sur le site du CEA/Saclay. Le candidat rejoindra l’équipe CalculS en charge du développement et du maintien des aspects HPC des modèles de climat de l’IPSL. La bibliothèque XIOS est développée et maintenue au sein de cette équipe d’ingénieurs.

#### Mission 
Dans le cadre du projet Européen du centre d’excellence ESIWACE2 (HPC climat), le Laboratoire des Sciences du Climat et de l&#39;Environnement (CEA/LSCE) un(e) ingénieur(e) (CDD 24 mois) pour faire évoluer XIOS. 
Les missions du poste s’inscrivent dans le cadre des délivrables attendus à travers les différents « work package » du projet ESIWACE2 pour lesquels XIOS est impliqué (WP 1, WP4, WP5):

* Passage à l’échelle de la bibliothèque sur des configurations de modèle « système terre » à très haute résolution (entre 10 km et 1 km de résolution en global) et sur plusieurs dizaines de milliers de cœurs de calcul (O(10 000)-O(100 000)). Cela implique l’optimisation des performances en calcul brut du code, la diminution de l’empreinte mémoire, l’optimisation des transferts de données à travers le réseau d’interconnexion (transferts MPI) afin d’améliorer la scalabilité, et l’optimisation des écritures parallèles des I/O à travers les bibliothèques parallèles NETCDF4-HDF5-MPIIO.

* Développement d’un module XIOS permettant la gestion des grands ensembles de simulation climatiques. La modélisation du climat se tourne de plus en plus vers l’exécution simultanée d’ensembles de plusieurs dizaines de simulations paramétriques (sensibilité à l’état initial, sensibilité aux paramètres, etc.) afin d’évaluer la variabilité des modèles. Le challenge consiste à absorber le flux de données en sortie des simulations et d’effectuer « In Situ » leur réduction statistique (moyenne, écart type, etc…) avant de les écrire sur disque de façon à en alléger considérablement le volume. Nous proposons que cette gestion « In-Situ » se fasse à travers XIOS.

#### Activités principales
L&#39;activité du poste implique une forte composante « développement » qui nécessite une solide expérience en développement applicatif C++ et un savoir-faire en programmation parallèle (MPI, OpenMP). Ce savoir-faire en programmation parallèle pourra éventuellement être renforcé  au cours du contrat. D&#39;autre part, la gestion et le maintien des sources sous le gestionnaire de sources SVN devront être assurés. Nous souhaitons également améliorer la documentation existante, ce qui nécessitera le goût et la capacité à rédiger en Anglais. Enfin, un « support » devra être assuré aux utilisateurs de XIOS, à la fois en les aidant à intégrer la bibliothèque dans leurs modèles mais aussi en suivant de près son utilisation (remontée de bugs, corrections,…). 


### Formation / compétences / qualités :
##### Compétences indispensables :

* Excellent niveau en C++.
* Expérience en développement d&#39;applications scientifiques.

##### Compétences appréciées (par ordre d&#39;importance) :

* Maitrise du parallélisme en mémoire distribuée (MPI).
* Maitrise du parallélisme en mémoire partagée, programmation multithread (OpenMP).
* Expérience en calcul intensif sur super-calculateur (connaissance du monde UNIX, gestionnaires de ressource, calculs parallèles).
* Connaissance du Fortran.
* Programmation sur accélérateur de calcul (GPU, OpenACC).
* Bon niveau d&#39;Anglais.
* Goût pour la rédaction (documentation).
* Gestion de codes source sous Subversion (SVN). 

**Lieu** :  CEA/LSCE
**Salaire brut mensuel** :  Entre 2500 et 3000€ selon expérience

**Postulez sur le site du CNRS** : <https://emploi.cnrs.fr/Offres/CDD/FR636-EVEMAG-032/Default.aspx>
**Contact pour toutes informations complémentaires :**  Yann Meurdesoif, <yann.meurdesoif@cea.fr>
