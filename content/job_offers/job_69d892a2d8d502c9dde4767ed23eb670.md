Title: Approche hybride simulation-apprentissage pour l’interaction fluide-structure dans un contexte industriel
Date: 2021-06-08 14:31
Slug: job_69d892a2d8d502c9dde4767ed23eb670
Category: job
Authors: Iraj MORTAZAVI
Email: iraj.mortazavi@lecnam.net
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: CNAM Paris, Altair Antony
Job_Duration: 36 mois
Job_Website: 
Job_Employer: CNAM Paris
Expiration_Date: 2021-06-11
Attachment: job_69d892a2d8d502c9dde4767ed23eb670_attachment.pdf

(voir plus de détails dans le document joint)

URGENT: date limite de candidature 11/06/2021 

Le but principal de cette thèse est de construire un couplage simulation/apprentissage (générique et adaptable à des outils pré-existants) pour prédire à moindre coût l’évolution temporelle des champs fluides et solides dans une configuration particulière : on cherchera ainsi à modéliser le couplage entre un écoulement de fluide instationnaire et une membrane déformable placée à l’intérieur d’un canal. Les outils numeriques utilisés dans le cadre de la thèse seront Altair Acusolve pour la partie fluide et Altair Optistruct pour la mécanique des solides. Les deux solveurs utilisent la méthode des éléments finis pour résoudre les EDP concernées. L’outil developé devra être en capacité d’interagir avec n’importe quel solveur existant.

Niveau : Master 2 et/ou diplôme d’ingénieur en mécanique numérique ou calcul scientifique.

Les compétences recherchées pour ce poste : calcul scientifique, mécanique des fluides numérique, calcul de structures, méthodes numériques. Des compétences en apprentissage automatique et/ou modèles réduits (réseaux de neurones etc...) seraient également appréciées.

Date du début : Novembre ou Décembre 2021 
