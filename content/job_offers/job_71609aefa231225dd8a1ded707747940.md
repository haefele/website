Title: Développeur Python
Date: 2021-06-04 10:10
Slug: job_71609aefa231225dd8a1ded707747940
Category: job
Authors: Romain Tavenard
Email: romain.tavenard@univ-rennes2.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Rennes, France
Job_Duration: 12 mois
Job_Website: https://rtavenar.github.io/research/projects/tslearn_position.html
Job_Employer: Université de Rennes 2
Expiration_Date: 2021-08-27
Attachment: 

Poste de développeur pour la librairie Python de machine learning pour les séries temporelles tslearn. N&#39;hésitez pas à prendre contact pour obtenir de plus amples informations.

Description (en anglais) ci dessous :

# General information

- Contract type : Fixed-term contract (1 year)
- Level of qualifications required : MSc or PhD
- Flexibility: part-time could be possible
- Place: Université de Rennes 2, Rennes, France
- Starting date: to be discussed
- Contact: Romain Tavenard firstname.lastname@univ-rennes2.fr

# Context

Have you ever heard of tslearn? tslearn is a collaborative open-source project which aims at providing machine learning tools for the analysis of time series datasets. It already features implementation of numerous state-of-the-art methods in machine learning for time series. However, such a library needs constant improvement in order to keep up with the state-of-the-art. More specific development are also needed in order to integrate new learning tasks, such as time series forecasting. Finally, as such, tslearn relies on numpy/Cython/numba and has an optional dependency on tensorflow for one of its subpackages. Better integration (possibly through the support of several backends) with modern tools that offer automatic differentiation would be a plus for such a library.

Collaborators for this project are the core development team of tslearn, as well as members of the Obelix group at IRISA (Rennes’ computer science lab) and the LETG lab (a lab whose focus is on remote sensing applications).

# Assignments

The successful candidate will be the main developer of the tslearn project for the duration of her contract. She will implement several new features in the library (new methods from the state-of-the-art or methods related to time-series-specific tasks that are not tackled in the library at the moment, such as forecasting for example) and work on the adaptation of existing code to modern backends (pytorch, tensorflow and/or jax, typically). She will also be in charge of managing the existing workforce and reviewing potential external contributions. Finally, she will participate in the promotion of the toolbox in order to expand its user base.

# Expected Skills

- A PhD or MSc in machine learning or signal processing.
- Publications or open-source software related to machine learning.
- Experience in programming with Python.
- Experience in collaborative projects hosted on platforms such as Github.
- Knowledge about machine learning tools for time series is a plus.
