Title: Ingénieur.e de recherche
Date: 2021-06-16 13:02
Slug: job_a9f9d708d3dac6a54effa0e3bca46d89
Category: job
Authors: Philippe Guillaume
Email: guillaume@modartt.com
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Toulouse
Job_Duration: 
Job_Website: http://www.modartt.com
Job_Employer: Modartt
Expiration_Date: 2021-06-30
Attachment: job_a9f9d708d3dac6a54effa0e3bca46d89_attachment.pdf

La société Modartt, leader mondial dans le domaine de la modélisation physique d’instruments de musique à clavier avec Pianoteq et Organteq, recrute un ou une &#34;jeune docteur&#34; en acoustique musicale ou mathématiques appliquées sur un poste d’Ingénieur de Recherche. La personne recrutée aura pour mission de créer et développer de nouveaux modèles d’instruments à vent, nécessitant en particulier une bonne culture musicale et un goût prononcé pour l’analyse du timbre musical. Les qualités recherchées sont l’imagination, la créativité et l’aptitude au travail d’équipe.

Mots clés : acoustique musicale, instruments à vent, systèmes dynamiques, traitement du signal, design sonore, programmation.