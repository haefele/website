Title: Ingénieur dans l&#39;équipe technique Grid&#39;5000
Date: 2021-01-18 15:38
Slug: job_ab51267128ddc615febdcfd91135bff3
Category: job
Authors: Simon Delamare
Email: simon.delamare@ens-lyon.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Lyon
Job_Duration: 31 mois
Job_Website: https://www.grid5000.fr
Job_Employer: Inria
Expiration_Date: 2021-04-12
Attachment: 

Nous recrutons un ingénieur en CDD pour rejoindre l&#39;équipe technique               
Grid&#39;5000 à Lyon.                                                                  

Nous sommes à la recherche de tout type de profil (administrateur                  
système ou développeur, junior ou expérimenté), pour s&#39;intégrer dans un            
environnement de travail &#34;devops&#34; avec une forte utilisation de                    
technologies GNU/Linux.                                                            

Pour plus d&#39;information et pour postuler, voir :                                   
<https://jobs.inria.fr/public/classic/fr/offres/2021-03272>                          

Vous pouvez également nous contacter directement par mail si vous                  
souhaitez plus d&#39;information.

Simon Delamare, Pierre Neyron
Directeurs de l&#39;équipe technique Grid&#39;5000
