Title: Analyse des interactions cardio-respiratoires chez le nouveau-né prématuré à base de modèles hybrides et de méthodes d’analyse de données
Date: 2021-05-19 14:45
Slug: job_af33d947288a45b75586b6b19864bf4e
Category: job
Authors: Virginie Le Rolle
Email: virginie.lerolle@univ-rennes1.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Rennes
Job_Duration: 36 mois
Job_Website: https://www.ltsi.univ-rennes1.fr/
Job_Employer: Université de Rennes 1
Expiration_Date: 2021-06-30
Attachment: job_af33d947288a45b75586b6b19864bf4e_attachment.pdf

**Contexte**
Le LTSI (Laboratoire Traitement du Signal et de l&#39;Image) est un laboratoire de recherche de l&#39;Université de Rennes 1, à l’interface des disciplines relevant des domaines des sciences et technologies de l’information et de la santé.

**Sujet**
En Europe, environ 8% des naissances sont prématurées et plus de 300 000 de nouveau-nés prématurés sont admis en soins intensifs néonatals (USIN) chaque année en Europe. Au cours de cette période, le risque de sepsis et d&#39;événements graves d&#39;apnée-bradycardie est important et peut être associé à une élévation de la mortalité et de déficiences neurocomportementales à long terme. 

L’objectif de la thèse est de proposer de nouvelles méthodes de traitement de données et de modélisation computationnelle, reposant sur une exploitation massive de données longitudinales issus de la base de données “CaressPremi”. Cette base de données est constituée de signaux cardio-respiratoires et d’annotations cliniques et doit permettre d’évaluer de nouveaux marqueurs multivariés pour la détection précoce du sepsis et d’évènements cardio-respiratoires chez le prématuré. 

Des méthodes de traitement du signal seront tout d’abord appliquées afin d’extraire des indices caractéristiques des interactions cardio-respiratoires. Des approches d’apprentissage automatique seront ensuite proposées et complétées par une méthodologie basée sur des modèles computationnels afin d’améliorer l’interprétation et l’explicabilité des analyses. Récemment, notre équipe a proposé des modèles hybrides intégrés complets, pour l&#39;analyse des réponses aiguës aux événements d&#39;apnée et d&#39;hypopnée, adaptés aux adultes, aux nouveau-nés à terme et prématurés. Ce modèle cardio-respiratoire pourra être amélioré afin d’étudier les mécanismes physiologiques liés au sepsis, puis des méthodes d’analyse paramétrique (sensibilité et identification) pourront être appliquées. 

**Profil**
Nous recherchons un candidat avec des compétences en analyse numérique, en traitement du signal et en programmation. Il sera nécessaire d’avoir une motivation pour l&#39;ingénierie biomédicale même si des connaissances en physiologie ne sont pas nécessaires.
