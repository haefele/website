Title: Algèbre linéaire rapide pour la CFD
Date: 2021-04-15 11:49
Slug: job_bcf14b6955ff1a0216c9bea1d3d33b3a
Category: job
Authors: Denis Gueyffier
Email: denis.gueyffier@onera.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: ONERA Chatillon
Job_Duration: 12 mois renouvelable
Job_Website: https://w3.onera.fr/formationparlarecherche/sites/w3.onera.fr.formationparlarecherche/files/pdoc-daaa-2021-06.pdf
Job_Employer: ONERA
Expiration_Date: 2021-07-08
Attachment: 

**Mots clés**
Algèbre linéaire rapide, méthodes de Krylov, préconditionnement, calcul HPC, CFD

**Profil et compétences recherchées**
Docteur en sciences dans le domaine des mathématiques appliquées ou de la mécanique des fluides numérique,
Compétences en algèbre linéaire numérique requises,
Expérience de la programmation parallèle en mémoire distribuée sur CPU multi-cœurs.

**Présentation du projet post-doctoral, contexte et objectif**
Vous participez aux travaux de recherche de l&#39;unité Conception et production de Logiciels pour les Ecoulements de Fluides (CLEF) du Département Aérodynamique, Aéroélasticité, Acoustique, DAAA de l’ONERA. La mission de cette unité est de réaliser des grandes plateformes logicielles de simulation en Mécanique des Fluides, aptes à répondre aux besoins de recherche et d&#39;applications de l&#39;ONERA, de laboratoires de recherche partenaires et des utilisateurs de l&#39;industrie.
Dans cette unité, vous participerez à des travaux de recherche en lien avec le développement du logiciel de simulation en aérodynamique SoNICS. Ces développements s’effectueront dans le cadre d’un vaste projet nommé SONICE, mené en collaboration entre Safran et l’ONERA. La recherche de points fixes des équations de Navier-Stokes est un enjeu majeur qui nécessite l&#39;inversion de très grands systèmes linéaires creux mal conditionnés. Pour ne pas avoir à former ni à stocker la totalité de la factorisation LU de la Jacobienne et pour assurer la robustesse de ces inversions, une méthode itérative de projection sur un espace de Krylov de type GMRES ou ORTHODIR est une nécessité. Pour assurer la bonne convergence des méthodes de Krylov, les stratégies de préconditionnement du système deviennent alors un enjeu majeur. Deux voies semblent aujourd’hui se dégager en matière de préconditionnement. La première est d’introduire un solveur itératif local à chaque sous domaine (FGMRES). La seconde consiste en la mise en place d’une agglomération Aggregation-based algebraic multilevel preconditioning (AMG) [1] dans laquelle les grilles grossières sont résolues par une méthode de  recouvrement de type Restricted Additive Schwarz (RAS) [2]. Un des principaux inconvénients de cette seconde approche est qu’elle induit un fort déséquilibre de charge au niveau HPC. Dans la continuité des travaux actuellement menés dans cette direction au sein de notre unité [3], vous mettrez en œuvre des méthodes de type Enlarged Krylov [4] en adaptant les optimisations de la décomposition QR proposées par Demmel et Grigori [5, 6]. Toujours dans le but de réduire le coût de la méthode, il sera demandé d’y introduire des algorithmes de déflation/restart efficaces. Vous aurez également en charge de développer des algorithmes distribués permettant un meilleur équilibrage des charges avec les méthodes d’agglomération.

Ces travaux s’inscriront aussi dans une action de recherche menée en collaboration entre Google AI et l’unité CLEF en vue d’obtenir une méthodologie de génération automatique de code pour la résolution en temps optimal de problèmes d’algèbre linéaire sur architectures HPC hétérogènes [7]. 

Les méthodes développées seront validées sur des cas bien documentés avant d’être éprouvées sur des configurations complexes en termes de géométrie ou de physique traitée. Les résultats feront l’objet de publications dans des journaux scientifiques.

[1] Y. Notay, Aggregation-based algebraic multilevel preconditioning, SIAM J. Matrix Anal. Appl., 27 (2006), pp. 998–1018
[2] X.-C. Cai and M. Sarkis, A Restricted Additive Schwarz Preconditioner for General Sparse Linear Systems, SIAM Journal on Scientific Computing (1999) 21:2, 792-797 
[3] N. Guilbert, Improvement of Large Sparse System Inversion for Computational Fluid Dynamics, Thèse de Doctorat en préparation à Sorbonne Université, Sciences mathématiques de Paris Centre depuis le 01/10/2018
[4] L. Grigori and O. Tissot, Scalable linear solvers based on enlarged Krylov subspaces with dynamic reduction of search directions, SIAM Journal on Scientific Computing, 41(5), C522–C547, 2019
[5] L. Grigori, S. Cayrols, and J. W. Demmel, Low Rank Approximation of a Sparse Matrix Based on LU Factorization with Column and Row Tournament Pivoting, SIAM Journal on Scientific Computing 2018 40:2, C181-C209
[6] O. Balabanov and L. Grigori, Randomized Gram-Schmidt process with application to GMRES (2020) ArXiv <https://arxiv.org/abs/2011.05090>
[7] U. Beaugnon, B. Clément, N. Tollenaere, A. Cohen, On the Representation of Partially Specified Implementations and its Application to the Optimization of Linear Algebra Kernels on GPU, ArXiv <https://arxiv.org/pdf/1904.03383>

Collaborations extérieures
Safran, INRIA projet Alpines, Google AI

Laboratoire d’accueil à l’ONERA
Département : DAAA
Lieu (centre ONERA) : Châtillon
Contact : Denis Gueyffier
Tél. : 01 46 73 37 43
Email : <denis.gueyffier@onera.fr>
