Title: Calcul haute performance d’homologie persistante par théorie de Morse discrète 
Date: 2020-11-17 16:21
Slug: job_c4235e319f0563937f1d229d0af515e9
Category: job
Authors: Julien Tierny
Email: julien.tierny@sorbonne-universite.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Paris
Job_Duration: 6 mois
Job_Website: https://julien-tierny.github.io/stuff/openPositions/internship2021b.pdf
Job_Employer: CNRS LIP6 - Sorbonne Université
Expiration_Date: 2021-02-09
Attachment: job_c4235e319f0563937f1d229d0af515e9_attachment.pdf

Dans le cadre d&#39;un projet ERC, nous recherchons activement un(e) étudiant(e) ayant un intérêt pour l&#39;Analyse Topologique de Données pour un stage de 6 mois niveau master 2 ou fin d&#39;école d&#39;ingénieurs, dans l&#39;optique d&#39;une continuation en thèse.

Le stage et la thèse portent sur la conception d&#39;algorithmes haute-performance en mémoire distribuée pour l&#39;analyse de données via le complexe de Morse-Smale.

Date de démarrage souhaitée: Avril 2020
Lieu du stage: Sorbonne Université, Paris (métro Jussieu)
 
Le sujet détaillé est disponible ci-dessous:
<https://julien-tierny.github.io/stuff/openPositions/internship2021b.pdf>
 
Julien Tierny et Pierre Fortin