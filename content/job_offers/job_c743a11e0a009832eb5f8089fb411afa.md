Title: Research support technician in Software engineering
Date: 2020-03-05 10:34
Slug: job_c743a11e0a009832eb5f8089fb411afa
Category: job
Authors: Xavier Besseron
Email: xavier.besseron@uni.lu
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Luxembourg
Job_Duration: 
Job_Website: https://recruitment.uni.lu/en/details.html?nPostingTargetId=62779
Job_Employer: University of Luxembourg
Expiration_Date: 2020-04-30
Attachment: job_c743a11e0a009832eb5f8089fb411afa_attachment.pdf

## Research support technician in Software engineering (M/F)

The University of Luxembourg is a multilingual, international research University.

The University of Luxembourg (UL) is seeking to hire for the Faculty of Science, Technology and Medicine (FSTM) a highly motivated and outstanding:

**Research support technician in Software engineering (M/F)**

* Ref: RCREQ0003766
* Permanent contract, 40 hours/week
* Status: Employee

### Your Role

The technical support team of the Department of Engineering (DES) is seeking to hire a software engineer to provide support to the research groups. The tasks involve developing, supporting and maintaining software systems. This includes maintenance, testing, quality control and programming of specific features of numerical simulation software, graphical user interfaces and pre/post-processing tools. The tasks also comprise the creation of software-related documentation, tutorials, the administration of computer systems and the participation in research projects and educational activities.

The position strengthens the technical support team for research projects and related teaching activities of the Department of Engineering and therefore contributes to the synergetic cooperation of the different institutes within the department. The tasks might be adapted or changed within the described task framework depending on any future change of the focus in research and education of the DES and the related institutes.

### Your Profile
- The candidate must possess at least Bachelor’s degree with working experience or a Master’s degree in Computer Science, Software Engineering or a related field
- Good knowledge in software engineering is requested. Experience is essential in the following areas:
    - Linux and command line environment
    - Strong programming and software design (object-oriented, ideally C++)
    - Software maintenance, compilation and continuous integration (ideally Git, Docker, CMake/CTest)
    - Design and programming of Graphical User Interface (ideally Qt)
- Experience in the following areas is an additional asset:
    - Scripting, Data Manipulation, Numerical languages (Python, R, Matlab, ...)
    - Familiar with Computational Mechanics and Numerical Methods
    - Cloud computing and/or High Performance Computing environments and portals
    - Documentation writing LaTeX, Doxygen, ...
- Industrial experience is an additional asset
- The candidate should be highly motivated to work in a team and to cooperate with other team members in interdisciplinary research projects of the DES and the related institutes
- Since the University of Luxembourg is an international, multi-lingual university, the candidate is required a good language proficiency in English. Knowledge of German, French or Luxembourgish would be a plus. The letter of motivation should be written in English

### Further Information

Please apply online until 30/04/2020 at <https://recruitment.uni.lu/en/details.html?nPostingTargetId=62779>.

The University of Luxembourg is an equal opportunity employer.
