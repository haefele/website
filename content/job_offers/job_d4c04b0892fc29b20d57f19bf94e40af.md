Title: Application Engineer
Date: 2021-07-02 14:29
Slug: job_d4c04b0892fc29b20d57f19bf94e40af
Category: job
Authors: Sonia Portier
Email: emploi@nextflow-software.com
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Nantes, France
Job_Duration: 
Job_Website: https://www.nextflow-software.com/
Job_Employer: Nextflow Software
Expiration_Date: 2021-09-24
Attachment: job_d4c04b0892fc29b20d57f19bf94e40af_attachment.pdf

Subsidiary of Siemens Digital Industry Software, Nextflow Software, is a Software Vendor headquartered in Nantes, France.
Nextflow develops and sells advanced Computer-Aided Engineering (CAE) software in the field of Computational Fluid Dynamics (CFD).
Nextflow Software addresses engineering companies developing and manufacturing products/systems and equipment involving fluid flows, potentially
with complex geometries and interactions with solids (e.g. moving parts, deformations), in the field of automotive, aerospace, marine, and many
other industries.

Thanks to its talented team of researchers and engineers, and based on more than 10 years of close partnership with leading academic
research laboratories from Ecole Centrale Nantes (ECN) and other universities, Nextflow Software is pushing the limits of hydrodynamics simulation.

**Your Role &amp; Responsibilities**
Your role is to support the technical relationship with prospects and customers during and after the sales process.
Reporting to the Application Engineering Manager, you will work in close collaboration with product management and development engineering.
You will also collaborate with the business development and sales team.
In this context, you will:

- Manage the technical relationship with accounts, before, during and after sales
- Work in close collaboration with business development managers to close new deals
- Conduct all technical pre-sales activities with prospects: demonstrations, presentations, technical exchanges, evaluations, benchmarks, proofs of concepts, etc.
- Attend technical conferences and tradeshows, either as visitor, speaker or exhibitor (booth duty)
- Work with partners in order to evaluate and build technical integrations between products or technologies
- Work in close collaboration with channel partner manager to train and assist the technical staff of distributors and resellers
- Provide post-sales technical support and professional services to customers such as studies, CFD computations, consulting, training
- Suggest evolutions and updates to product teams to better match market needs
- Assist in new products testing and validation
- Assist sales and marketing teams in building technical sales tools such as demonstrations, videos, presentations, competitive analysis, benchmarks, technical papers, articles, blogs, posts, etc.

**Your Profile**

Passionate with CAE and CFD software, you are eager to work in a startup environment where your contribution will have a direct impact on the business success and product direction.
You hold a Master of Science or PhD in CFD or other numerical simulation domains.
You have at least 3 years of experience in similar position with a CAD, CAE or CFD software vendor.
You are naturally curious and eager to solve complex engineering problems.

**Your skills:**

- Strong technical and business abilities including software sales techniques and processes
- Performance-oriented within a startup context
- Experience of numerical simulation, CAD, CAE, CFD and meshing methods and technologies
- Experience with CFD tools like SPH, STAR-CCM+, Fluent, CFX or others
- Fluent speaking and writing skills both in French and English
