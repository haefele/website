Title: Towards a fast and efficient domain decomposition approach to solving multiphase flow problems
Date: 2021-05-31 08:44
Slug: job_dc22cdd48bf0b157ec64b175e68f02f4
Category: job
Authors: Taraneh Sayadi
Email: taraneh.sayadi@sorbonne-universite.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Institut Jean-le-Rond d&#39;Alembert
Job_Duration: 12 mois
Job_Website: 
Job_Employer: Sorbonne Université
Expiration_Date: 2021-09-30
Attachment: 

# Context
With advances in both computational sciences and computing technology, computational fluid dynamics (CFD) is playing an ever increasing role in the prediction of complex flows involving multiple phases, combustion, turbulence and compressibility effects. In the case of multi-phase flows in particular, simulation and modelling have evolved from simplistic or back-of-the-envelope estimates (integral balance, analytical analysis) to exascale computing applications, generating unprecedented amounts of information. Nevertheless, the challenging nature of such flows and the complexity of the underlying equations (existence of discontinuities in the interface vicinity, for example) make their computation prohibitive and slow. The development of faster and more efficient preconditioners is an effective way to improve the scalability and reduce the run-time of the underlying numerical solvers. Another direction is to fully leverage the vectorisation and parallel capabilities of the latest hardware technologies, aiming to reach peak FLOPS performances. In this project, we propose the application of domain decomposition techniques to the preconditioning of the two-phase Navier-Stokes equations. The implementation of all developments will be performed in a recently developed high-level, high-performance dynamic programming language (Julia) that will be deployed to the latest generation of multicore processors targeting ultimately the ARM architecture of the Fujitsu A64FX processor.

# Project
The selected candidate will first focus on implementing domain decomposition techniques to solve the two-phase Stokes equation in an in-house Julia code. The performance of the code will be compared to conventional treatment of the solution already implemented in the code and the time to solution and speed up will be assessed. In the second stage, the importability of the resulting solver on the new generation A64FX ARM processor will be assessed.

# Profile
The candidate should have a PhD degree in fluid mechanics or applied mathematics, with experience in scientific computing.

# Skills
Experience with either computational fluid dynamics or numerical linear algebra is required. Prior experience with the Julia programming language, domain decomposition method and/or multiphase flows is welcome, although not required.

# Duration and start date
The position is offered for the duration of 12 months, starting September, 2021 or later.

# Salary
Gross monthly salary is 2,589.68€.

# Required documents

* CV,
* List of publications,
* Motivation letter.

# In collaboration with

* Frédéric NATAF (Sorbonne Université),
* Vincent LE CHENADEC (Université Gustave Eiffel),
* Pierre Fortin (Université de Lille).
 
