Title: Assistant Professor (tenure track) in Computer Science with specialisation in applied and/or foundational aspects of Machine Learning (M/F)
Date: 2020-02-27 09:03
Slug: job_e50cc60ba5d2375c88074bc4af51ce04
Category: job
Authors: Prof. Dr. Sjouke Mauw
Email: FSTM_recruitment_DCS_ML@uni.lu
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Esch-sur-Alzette, Luxembourg
Job_Duration: 5 years
Job_Website: http://emea3.mrted.ly/2ezfc
Job_Employer: University of Luxembourg
Expiration_Date: 2020-03-31
Attachment: 

The Faculty of Science, Technology, and Medicine (FSTM) of the University of Luxembourg (UL) announces an opening for an Assistant Professor (tenure track) in Computer Science with specialisation in applied and/or foundational aspects of Machine Learning (ML). The position is anchored within the Department of Computer Science (DCS). 
 
**Ref:** F1-50011608A (to be mentioned in all correspondence)
Full-time position (40 hrs/week)
Earliest starting date: September 2020
Fixed-term contract: 5 years, with perspective of a permanent position and promotion to Associate Professor upon a positive evaluation. In case of an excellent more senior candidate, a direct appointment at the level of Associate Professor may be considered.

**Your Role:**

The position at the Assistant Professor level follows Luxembourg’s newly introduced tenure-track scheme according to which the initial appointment is a fixed-term contract. Upon successful evaluation, the candidate will be offered a permanent position as Associate Professor.
 
To diversify the existing skills and expertise within DCS, the candidate is expected to:

*      advance state-of-the-art research in the area of Machine Learning,
*      offer focused Machine Learning courses at both Bachelor and Master level,
*      supervise PhD candidates and post-doctoral researchers at the University of Luxembourg, 
*      collaborate across the various departments within the FSTM (such as Mathematics, Physics and Materials Science, Engineering, Medicine, Computational and Life Sciences), with other faculties, as well as with the adjunct research centres (LCSB, C2DH, SnT, LIST). 
 
The initial package includes a PhD and a post-doctoral researcher position. The Luxembourgish research funding agency (FNR) offers a broad range of further funding opportunities through projects and special excellence programs (ATTRACT/PEARL). Academic salaries in Luxembourg are very competitive and among the highest in Europe.

**Your Profile:**

The FSTM is seeking to hire an outstanding and visionary candidate in the area of Machine Learning. Applicants should have an excellent international track record, a strong background in the theoretical foundations of Machine Learning, an interest in innovative Machine Learning techniques, and be open for interdisciplinary collaborations with both academia and industry. Strong candidates with a more practical orientation will also be considered. Non-exclusive possible areas of interest are Bayesian and statistical-relational learning, learning probabilistic and causal graphical models, learning other types of structured/logical knowledge, explainable learning, deep learning, unsupervised learning, big data applications (scalability, distributed and parallel execution of learning techniques), applications for natural-language processing and computer vision, autonomous systems, space research, and industry 4.0.
 
The University of Luxembourg is set in a multilingual context. The person hired on this position must be proficient in English. French or German would be an asset.
 
**We Offer:**

*      An international team at a young and dynamic university 
*      An interdisciplinary research environment 
*      A wide network of academic, institutional, and corporate partners 
*      Skilled support staff and a team-oriented work environment
*      Very competitive salary and benefits packages

**Applications:**

Applications should contain (in PDF format in English):

*      A motivation letter 
*      A detailed curriculum vitae
*      A 1-page research statement
*      A 1-page teaching statement
*      A full list of publications
*      In addition, applicants are asked to provide recommendation letters of at least 3 referees which are to be submitted per email to <DCS_Head@uni.lu> on or before the application deadline (March 31st, 2020)

For any questions please contact: Head of Department Professor Dr. Sjouke Mauw (e-mail: <DCS_Head@uni.lu>).
All applications must be sent per e-mail on or before March 31st, 2020 to: <FSTM_recruitment_DCS_ML@uni.lu>
 
All applications will be handled with strict confidence.
 
The University of Luxembourg is an equal opportunity employer. Special consideration will be given to female and young candidates to address the age pyramid and gender balance within DCS. 



 
