Title: Calibration d&#39;un modèle multi-physique de Batterie par la Différentiation Automatique
Date: 2021-04-19 09:45
Slug: job_ecedae8b48fbe4fc815a06ec5ea75ae7
Category: job
Authors: Erwan ADAM
Email: erwan.adam@cea.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Saclay
Job_Duration: 1 an
Job_Website: http://www-instn.cea.fr/formations/formation-par-la-recherche/post-doctorat/liste-des-propositions-de-post-doctorat/calibration-dun-modele-multi-physique-de-batterie-par-la-differentiation-automatique,21-0037.html
Job_Employer: CEA Saclay
Expiration_Date: 2021-07-12
Attachment: 

**DOMAINE DE RECHERCHE**
Mathématiques - Analyse numérique - Simulation / Sciences pour l’ingénieur

**INTITULÉ DE LA PROPOSITION**
Calibration d&#39;un modèle multi-physique de Batterie par la Différentiation Automatique

**RÉSUMÉ DE LA PROPOSITION**
La modélisation et la simulation numérique deviennent des outils de plus en plus indispensables à la compréhension des mécanismes physico-chimique des systèmes électrogènes, et en particulier des batteries Li-Ion. En effet, ces outils permettent d’améliorer la compréhension des mécanismes physico-chimique, mécanique et thermique, et ainsi capitaliser sur les caractérisations, mais également d’optimiser les designs des matériaux, des électrodes et des cellules, ainsi que la gestion de la batterie sur sa durée de vie complète.

Le projet s&#39;inscrit dans le cadre de la calibration des paramètres d&#39;un modèle de batterie Li-Ion à l&#39;échelle &#34;électrode&#34;, modèle de Newman, à partir d&#39;une base réduite d&#39;essais expérimentaux. Ce modèle permet de simuler les performances de la batterie, tout en ayant accès aux concentrations et potentiels locaux au sein des électrodes et de la matière active. Basé sur des paramètres physiques, ce modèle est difficile à paramétrer, car les propriétés des matériaux et des électrodes sont soit mal connues, soit difficilement mesurables.

Afin d&#39;accélérer la calibration du modèle, nous comptons utiliser la méthode des &#34;Actives-Subspaces&#34; pour identifier les paramètres d&#39;entrées du modèle les plus influents (réduction de dimensions). Pour cela, il nous faut évaluer en plusieurs points du domaine d&#39;étude les gradients de la fonction ou du code de calcul. Mais au lieu d&#39;utiliser une méthode classique de différences finies, nous souhaitons mettre en place dans la plateforme URANIE Différentiation Automatique à travers la librairie &#34;Mission : Impossible&#34; développée au CEA/List. Ainsi, à chaque évaluation du modèle, nous aurons à notre disposition, non seulement la valeur du modèle, mais aussi ses gradients et toutes autres dérivées d&#39;ordres supérieurs (comme la hessienne) de manière exacte.

Finalement, une calibration complète du modèle de Newman sera réalisée sur un cas d&#39;application connu en limitant le nombre d&#39;expériences utilisées.

**INFORMATIONS PRATIQUES**
Département de Modélisation des Systèmes et Structures
Service de Thermohydraulique et de Mécanique des Fluides
Laboratoire de Génie Logiciel pour la Simulation
