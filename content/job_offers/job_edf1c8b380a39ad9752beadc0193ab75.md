Title: Mise en œuvre d’un logiciel coupleur pour des problèmes multi-physiques 
Date: 2020-09-30 16:23
Slug: job_edf1c8b380a39ad9752beadc0193ab75
Category: job
Authors: Bernard Vialay, Daniele Colombo et Ani Anciaux-Sedrakian 
Email: ani.anciaux-sedrakian@ifp.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: IFPEN (Rueil-Malmaison) et Andra (Chatenay-Malabry)
Job_Duration: 12 mois
Job_Website: 
Job_Employer: IFPEN - ANDRA
Expiration_Date: 2020-12-23
Attachment: job_edf1c8b380a39ad9752beadc0193ab75_attachment.pdf

La mise à disposition croissante de codes de simulation open-source incite à étudier comment les utiliser pour construire des applications multi-physiques au travers d’un outil de couplage capable de gérer les transferts d’informations entre les codes, afin de mettre en place un schéma de couplage adapté aux phénomènes physiques concernés.

Dans la simulation d’applications multi-physiques, le temps de calcul est un enjeu majeur et nécessite l’utilisation du calcul haute performance. Dans ce contexte, l’algorithme de couplage est basé sur l&#39;échange et le traitement d&#39;informations entre deux ou plusieurs codes parallèles s’appuyant sur des grilles de calcul, des algorithmes et des discrétisations temporelles différentes selon le phénomène physique à simuler.

Les transferts d’informations doivent être pensés pour maintenir l&#39;évolutivité du code tout en assurant la précision numérique. Le transfert de données d&#39;une interface distribuée vers une autre sur un très grand nombre de cœurs de calcul est un défi majeur et les solutions existantes ne sont pas encore optimales.
Dans le contexte de la simulation géomécanique, une preuve de concept de couplage entre un code de simulation mécanique et un code de simulation d’écoulement en milieu poreux a été mise en œuvre à IFPEN. Après une première étape d’analyse de cet existant, l’objectif du post-doctorat sera d’y apporter une double rupture, afin de lever les verrous de performance numérique et informatique de ce schéma de couplage, ainsi que d’en améliorer sa robustesse et si possible son évolutivité.

- Tout d’abord, la solution en place aujourd’hui repose sur un couplage par fichiers, ce qui peut imposer d’importantes latences sur les transferts de données. Le premier défi sera donc de mettre en place une technologie moderne de couplage permettant de lever cette première limitation. Ce développement devra également permettre de mieux isoler l’interface de couplage entre les codes et de la rendre plus facilement évolutive.
- Le schéma de couplage maître-esclave séquentiel actuellement en place n’offre qu’une faible scalabilité parallèle. La seconde limitation que nous souhaitons lever concerne donc la performance du couplage lors d’un calcul parallèle sur les architectures modernes. La vraie difficulté de ce nouveau défi est que, si le coupleur mis en place pourra fournir les technologies parallèles adaptées, ses capacités parallèles ne pourront être exploitées sans modifier l’algorithme de couplage numérique existant, ce qui s’avère être très intrusif dans les codes couplés. Le post-doctorant devra donc étudier la possibilité de mettre en place de nouveaux algorithmes numériques de couplage. Ses travaux devront permettre de comprendre s’il est possible de trouver un nouvel algorithme plus propice à une décomposition parallèle et qui, appliqué à notre cas de couplage, donne les résultats attendus de façon robuste. Le post-doctorant étudiera pour cela la possibilité de modifier le schéma de communication entre les méthodes couplées. Il pourra également réfléchir à la mise en place de nouveaux algorithmes de couplage en remplacement de la méthode de point fixe utilisée actuellement.

Programme du post-doc (en fonction de la durée) :

- Etude bibliographique des coupleurs (ex : OpenPalm, Padawan, Precice …) ;
- Familiarisation avec les simulateurs à coupler et le code couplé existant, ainsi que avec la physique des phénomènes couplés ;
- Identification d’un logiciel coupleur pertinent ;
- Réalisation d’un POC pour le cas d’usage défini :
- Couplage géomécanique entre un code d’écoulement en milieu poreux, et un simulateur mécanique, comme le Code_Aster.
- Etude de l’implantation d’un schéma de communication direct à la place de celui centralisé ;
- Etude de l’utilisation d’algorithmes itératifs de couplage plus performants que celui du point fixe pour les problèmes non-linéaires.

Niveau requis : connaissances en C/C++, Fortran 90/2003, Python, analyse numérique, couplage de codes, Linux
