Title:  Modélisation multi-échelle de l’évolution des propriétés élastiques et diffusives d’un élastomère endommagé par décompressions répétées d’hydrogène
Date: 2020-02-18 14:56
Slug: job_fbb43efd90f95b313eb0932197a34bc2
Category: job
Authors: Nait-ali Azdine
Email: azdine.nait-ali@ensma.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Poitiers
Job_Duration: 
Job_Website: 
Job_Employer: Institut Pprime 
Expiration_Date: 2020-05-12
Attachment: job_fbb43efd90f95b313eb0932197a34bc2_attachment.pdf


**Contexte et enjeux**

Le projet de thèse concerne un type d’endommagement récurrent dans les polymères exposés à un gaz diffusant : la nucléation et la croissance de multiples cavités et/ou fissures sous décompression. Le phénomène résulte de l’expansion locale du gaz préalablement absorbé, lorsque la désorption hors du polymère est trop lente par rapport à la décompression imposée. Cette question générale est traitée depuis plusieurs années dans l’équipe « Endommagement et Durabilité » de l’Institut Pprime, majoritairement sur des élastomères exposés à l’hydrogène gazeux sous forte pression. Ce phénomène peut en effet affecter des composants d’étanchéité de structures de stockage et de distribution d’hydrogène gazeux pour les véhicules à pile à combustible, de plus en plus déployés comme moyens de mobilité propre, et dont il faut garantir la fiabilité.
Selon sa morphologie et son ampleur, qui dépendent de nombreux facteurs (pression d’exposition, conditions de décompressions, état mécanique,...), cet endommagement peut significativement affecter la rigidité du composant élastomère, et induire une dégradation prématurée de sa perméabilité au gaz. Ce dernier point est particulièrement délicat dans le cas d’applications concernant l’hydrogène. Il y a donc un enjeu important à comprendre et prendre en compte cet endommagement dans la conception des composants utilisés dans les applications mobiles. Pour ces usages, les composants voient en outre des décompressions répétées liées aux phases de remplissage – vidange. Ces cycles de pression font évoluer la morphologie de l’endommagement. Il est donc crucial de bien capter la notion de cumul dans des outils de prédiction. Aucune caractérisation des conséquences de cet endommagement cumulé sur la tenue mécanique et la perméabilité n’a jusqu’ici été conduite dans les élastomères sous environnement hydrogène.

**Objectifs**

L’objectif de la thèse est de traiter cette question sous un angle assez fondamental. Dans un élastomère non renforcé utilisé pour formuler certains composants d’étanchéité sous hydrogène, on s’intéressera d’abord à la façon dont le dommage se cumule et évolue au cours de cycles de pression liées à des phases de remplissage / vidange répétées, puis aux conséquences de cet endommagement sur les propriétés mécaniques et la perméabilité, et enfin à la modélisation multi-échelles de cet abattement de propriétés (module mécanique et propriétés de sorption) pour en déduire des lois utilisables pour la conception et l’évaluation de la fiabilité cycle après cycle.

**Programme de travail**

Dans un premier volet expérimental, l’élastomère sélectionné sera exposé à des cycles de pression répétés, dans un état libre de contraintes et sous compression uniaxiale, plus proche des conditions de service d’un joint d’étanchéité. Très peu de laboratoires académiques dans le monde sont en mesure de conduire des essais mécaniques sous forte pression d’hydrogène. L’Institut Pprime est l’un d’eux. Il dispose à la fois de plusieurs machines d’essais mécaniques équipées d’enceintes sous pression d’hydrogène dotées d’instrumentation spécifique et d’un essai unique de tomographie in-situ qui permet de quantifier proprement la morphologie de l’endommagement dans le volume de l’échantillon au cours du temps, pendant et après la décompression. Une analyse tomographique du champ l’endommagement induit sera conduite après chaque cycle. La réponse mécanique sous différents trajets de chargement, ainsi que les propriétés de perméabilité seront également mesurées après un nombre croissant de décompressions préalables.
Sur la base de ces essais, l’objectif majeur de la thèse est de faire progresser un modèle mécanique analytique multi-échelles en intégrant des formes de distribution caractéristiques de la décompression sous gaz : la formation de clusters de cavités et la coexistence entre cavités et fissures. Un modèle non-local développé depuis plusieurs années au laboratoire fournit un cadre permettant de prendre en compte plusieurs longueurs caractéristiques, et d’estimer, par transition d’échelles, les propriétés élastiques et diffusives macroscopiques pour un nombre croissant de décompressions et déduire des lois d’abattement de ces propriétés avec le cyclage. Les différentes morphologies d’endommagement seront introduites par complexité croissante. Là où un grand nombre d’études de la littérature travaillent sur des matériaux virtuels générés numériquement et dont la morphologie d’endommagement peut s’avérer idéaliste, a fortiori dans le cas d’un comportement non-local, une deuxième originalité forte du projet est de travailler sur des champs de cavités réels obtenus à partir d’acquisitions tomographiques sur matériaux réels ayant subi plusieurs décompressions rapides. La prise en compte d’un champ complet de cavités est en effet cruciale lorsque la distribution de ces dernières n’est pas purement aléatoire [1-2] et comporte, comme c’est le cas ici, des défauts clustérisés ainsi que des chemins de cavitation privilégiés.
Un préalable à cette approche par transition d’échelle est d’identifier la taille d’un Volume Représentatif. Les études antérieures dans un EPDM exposé à des pressions allant jusqu’à 30 MPa [3] ont montré qu’un Volume Élémentaire Représentatif morphologique inclut un grand nombre de cavités. Dans la littérature, les calculs mécaniques prenant en compte la cavitation et ses effets sont réalisés seulement à l’échelle de la cavité et non pas sur un champ de cavités. Une raison évidente de cette limitation est la taille des données difficilement manipulable, et la génération de maillages très vite coûteuse en temps et surtout en capacité de calculs, particulièrement dans le cas des éléments finis. C’est pourquoi dans cette étude, le doctorant utilisera un modèle macroscopique déterministe homogène, des simulations à champs complets à l’aide d’un solveur Fast-Fourier-Transform (FFT) basé sur les travaux de [4] et développé en interne.
Le modèle sera validé sur un autre élastomère soumis à des décompressions répétées similaires.

1. O. Kane-diallo, A. Nait-Ali, S. Castagnet: Catching the time evolution of microstructure morphology from dynamic covariograms. Comptes Rendus Mecanique (2014)
2. S. Castagnet, D. Mellier, A. Nait-Ali, Guillaume Benoit: In-situ X-ray computed tomography of decompression failure in a rubber exposed to high-pressure gas. Polymer Testing (2018).
3. O. Kane-Diallo, S. Castagnet, A. Nait-Ali, G. Benoit, J.C. Grandidier: Time-resolved statistics of cavity fields nucleated in a gas-exposed rubber under variable decompression conditions - Support to a relevant modeling framework. Polymer Testing (2016)
4. H. Moulinec, P. Suquet, A numerical method for computing the overall response of nonlinear composites with complex
microstructure, Comput. Method. Appl. M. 157 (1998)
