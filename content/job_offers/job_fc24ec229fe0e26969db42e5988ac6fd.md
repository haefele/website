Title: Maitre de Conférences Contractuel 
Date: 2021-06-08 14:23
Slug: job_fc24ec229fe0e26969db42e5988ac6fd
Category: job
Authors: Luisa Silva
Email: luisa.rocha-da-silva@ec-nantes.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Nantes
Job_Duration: 
Job_Website: https://www.ec-nantes.fr/english-version/recruitment/lecturer-in-calcul-scientifique-haute-performance-apprentissage-automatique-et-analyse-massive-de-donnees
Job_Employer: Ecole Centrale de Nantes
Expiration_Date: 2021-07-04
Attachment: job_fc24ec229fe0e26969db42e5988ac6fd_attachment.pdf

Recrutement d’un **Maître de Conférences Contractuel (H/F)**
Champ disciplinaire : **sections 27, 60 du C.N.U.**
Profil court : **Calcul scientifique haute performance / Apprentissage automatique et analyse massive de données / Mécanique numérique**

(plus de détails dans le document joint)

**Profil attendu :**

*Compétences techniques*
Doctorat en informatique / mécanique numérique avec une expertise dans un ou plusieurs de ces domaines :

- Calcul scientifique massivement parallèle,
- Méthodes de réduction de dimension, en analyse de données et pour le développement de solutions paramétriques,
- Expérience dans le développement de logiciels et dans l’utilisation de supercalculateurs,
- Maîtrise des principes et algorithmes de l’apprentissage et connaissance d’outils informatiques existants.

*Mots-clefs :* calcul parallèle, jumeau numérique, apprentissage automatique, mécanique numérique.

*Informations relatives au poste :*
Nature du contrat : CDD 1 an puis CDI
Date de prise de poste : Novembre 2021
Rémunération : Grille des maitres de conférences
Date de fin de candidature : 04/07/2021
