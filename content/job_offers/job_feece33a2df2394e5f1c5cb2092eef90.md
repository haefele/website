Title: Modèles Auto-associatifs pour la Dispersion de Polluants dans l&#39;Atmosphère
Date: 2020-01-28 15:33
Slug: job_feece33a2df2394e5f1c5cb2092eef90
Category: job
Authors: Sylvain Girard
Email: girard@phimeca.com
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Paris
Job_Duration: 12 mois
Job_Website: http://www.inria.fr
Job_Employer: Inria et Phimeca Engineering
Expiration_Date: 2020-04-21
Attachment: job_feece33a2df2394e5f1c5cb2092eef90_attachment.pdf

Le projet MADiPA collaboration entre INRIA et Phimeca Engineering, subventionné par l&#39;AMIES vise a développer une méthode de prévision de la dispersion de polluants dans l&#39;atmosphère.

L&#39;approche envisagée consiste à construire une approximation d&#39;un modèle physique coûteux en utilisant des modèles Auto-Associatifs pour contourner la difficulté liée à la grande dimension des entrées et sorties.

Voir le fichier joint pour davantage de détails.