# -*- coding: utf-8 -*-
""" Adding custom filters """

import datetime
from pelican import signals
from pelican.utils import get_date

def date_time(dt):
    """ Format a date with time if not 00h00 """
    s = dt.strftime('%-d %B %Y')
    if dt.hour != 0 or dt.minute != 0 or dt.second != 0:
        s += ", %s" % dt.strftime("%-Hh%M")
    return s


def date_interval(dt_interval):
    """Format a date interval of an event."""
    (start_date, end_date) = dt_interval

    if start_date.year != end_date.year:
        return "Du %s au %s" % (start_date.strftime('%d %B %Y'), end_date.strftime('%d %B %Y'))
    elif start_date.month != end_date.month:
        return "Du %s au %s" % (start_date.strftime('%d %B'), end_date.strftime('%d %B %Y'))
    elif start_date.day != end_date.day:
        return "Du %s au %s" % (start_date.strftime('%d'), end_date.strftime('%d %B %Y'))
    else:
        return "Le %s" % date_time(start_date)


def highlighted_article(articles, config):
    """Select the article that is highlighted"""
    try:
        if config['article'] == 'date':
            return next(article for article in sorted(articles, key=lambda a: a.date, reverse=True)
                        if article.category.slug in config['categories'] and not article.is_finished)
        elif config['article'] == 'next':
            return next(article for article in reversed(articles) if article.category.slug in config['categories']
                        and not article.is_finished)
        else:  # Slug specified
            return next(article for article in articles if article.slug == config['article']
                        and not article.is_finished)
    except StopIteration:
        try:
            return next(article for article in articles if article.category.slug in config['categories'])
        except:
            # Articles may be empty or not contains any valid category due to pagination, archives, ...
            return articles[0]


def previewed_articles(articles):
    """Select the articles that will be displayed on homepage preview"""

    max_counter = {"job": 1, 'journee': 2, 'formation': 2, 'cafe': 2}
    max_selection = 5

    while True:

        selection = []
        category_counter = {}

        for article in filter_by_date(articles, 'expiration_date'):
            if article.category in max_counter.keys():
                counter = category_counter.get(article.category, 0)
                if counter < max_counter[article.category]:  # Limit to 2 articles by category
                    selection.append(article)
                    category_counter.update({article.category: counter + 1})

            # Stop here because we display only 5 article previews
            if len(selection) >= max_selection:
                return selection[:max_selection]

        # There is enough articles to fill the preview => increase all counters
        if len(articles) >= max_selection:
            for category in max_counter.keys():
                max_counter[category] += 1

        else:  # Number of total articles is less than max preview
            return selection


def group_by_year(articles_gen):
    """Group articles by starting year"""

    articles = list(articles_gen) # Hugly workaround when given article list is a generator (e.g. input from another filter)
    years = sorted(list(set(map(lambda a: a.start_date.year, articles))), reverse=True)
    return [(year, [a for a in articles if a.start_date.year == year]) for year in years]


def filter_by_date(articles, field_name, date = None):
    """Keep only articles with given date field that is in the future compared to an (optionaly) given date"""

    if date is None:
        date = datetime.datetime.now().strftime('%Y-%m-%d')

    def predicate(a):
        try:
            return getattr(a, field_name) >= get_date(date)
        except:
            return True

    return [a for a in articles if predicate(a)]


def add_filter(pelican):
    pelican.env.filters.update({
        'date_time': date_time,
        'date_interval': date_interval,
        'highlighted_article': highlighted_article,
        'previewed_articles': previewed_articles,
        'group_by_year': group_by_year,
        'filter_by_date': filter_by_date,
    })


def register():
    signals.generator_init.connect(add_filter)
