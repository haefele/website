{% if header %}
<div class="storage">
    <div class="card">
        <div class="card-header">
            <h6>
                {{ storage['name'] }}
            </h6>
        </div>
        <div class="card-body row">
            {% if storage['typename'] %}
                <p class="col-sm"><img src="../theme/img/storage_type.png" alt="Type de stockage" title="Type de stockage" />
                    {{ storage['typename'] }}
                </p>
            {% else %}
                <p class="col-sm"><img src="../theme/img/storage_type.png" alt="Type de stockage" title="Type de stockage" />
                </p>
            {% endif %}
            {% if storage['filesystemtype'] %}
                <p class="col-sm"><img src="../theme/img/filesystem.png" alt="Système de fichiers" title="Système de fichiers" />
                    {{ storage['filesystemtype'] }}
                </p>
            {% else %}
                <p class="col-sm"><img src="../theme/img/filesystem.png" alt="Système de fichiers" title="Système de fichiers" />
                </p>
            {% endif %}
            {% if storage['size'] %}
                <p class="col-sm"><img src="../theme/img/storage.png" alt="Volumétrie" title="Volumétrie" />
                    {{ storage['size'] | format_storage(4) }}
                </p>
            {% else %}
                <p class="col-sm"><img src="../theme/img/storage.png" alt="Volumétrie" title="Volumétrie" />
                </p>
            {% endif %}
        </div>
        <div class="card-footer row">
        </div>
{% endif %}
{% if footer %}
    </div>
</div>
{% endif %}
