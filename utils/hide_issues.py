"""Use python-gitlab API to make all project issues confidential"""

from follow_transfer import connect_to_gitlab, GITLAB_PROJECT_ID

gl = connect_to_gitlab()

project = gl.projects.get(GITLAB_PROJECT_ID)
project_issues = project.issues.list(all=True, lazy=True)

for issue in project_issues:
    issue.confidential = True
    issue.save()