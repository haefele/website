#!/usr/bin/env python3
"""
Returns the list of expired job offers 

To be used with following bash command that remove listed files:

IFS=$'\n'; for i in $(utils/list_expired_job_offers.py); do git rm "$i"; done

"""

import glob
import re
import datetime
import time

JOB_PLACEHOLDER_NAME = 'job_placeholder.md'


def get_job_offer_list(path):
    max_date = datetime.datetime.now() - datetime.timedelta(1)
    file_paths = glob.glob('{}/*.md'.format(path))

    for file_path in file_paths:
        if os.path.basename(file_path) != JOB_PLACEHOLDER_NAME:
            content = open(file_path).read()
            category = re.search(r"^Category: (.+)", content, re.M | re.I).group(1)
            if category == 'job':
                expiration_date = datetime.datetime.strptime(
                    re.search(r"^Expiration_Date: ([\d-]+)", content, re.M | re.I).group(1),
                    '%Y-%m-%d'
                )
                if expiration_date < max_date:
                    yield file_path
                    attachment = re.search(r"^Attachment: (.*)", content, re.M | re.I).group(1)
                    if attachment:
                        dir_path = os.path.dirname(file_path)
                        yield os.path.join(dir_path, attachment)


if __name__ == '__main__':
    import os
    default_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../content/job_offers'))

    import argparse
    parser = argparse.ArgumentParser(description="Returns the list of expired job offers")
    parser.add_argument('--path', metavar='PATH', help='Job offers folder path', default=default_path)
    args = parser.parse_args()

    for f in get_job_offer_list(args.path):
        print(f)
